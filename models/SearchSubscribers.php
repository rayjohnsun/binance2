<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Subscribers;

/**
 * SearchSubscribers represents the model behind the search form of `app\models\Subscribers`.
 */
class SearchSubscribers extends Subscribers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['imya', 'email', 'page', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subscribers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $s_text = $this->created_at;
        if (strlen($this->created_at) > 10) {
            $s_text = date('Y-m-d', strtotime($this->created_at));
        }

        $query->andFilterWhere(['like', 'imya', $this->imya])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'page', $this->page])
            ->andFilterWhere(['like', 'created_at', $s_text]);

        return $dataProvider;
    }
}
