<?php

    namespace app\models;

use app\components\NHelper;

    use Yii;

    class Bdata extends \yii\db\ActiveRecord {

        const PAGE_INDEX        = 0;
        const PAGE_BUY          = 1;
        const PAGE_MAIN         = 2;
        const PAGE_MINING       = 3;
        const PAGE_TRADE        = 4;
        const PAGE_CARDS        = 5;
        const PAGE_ITEM_CARD    = 6;
        const PAGE_ITEM_CARD2   = 7;
        const PAGE_BLOG         = 8;
        const PAGE_ABOUT        = 9;

        private static $pages = [
            0 => 'PAGE_INDEX',
            1 => 'PAGE_BUY',
            2 => 'PAGE_MAIN',
            3 => 'PAGE_MINING',
            4 => 'PAGE_TRADE',
            5 => 'PAGE_CARDS',
            6 => 'PAGE_ITEM_CARD',
            7 => 'PAGE_ITEM_CARD2',
            8 => 'PAGE_BLOG',
            9 => 'PAGE_ABOUT',
        ];

        public static function getPageById($id) {
            return isset(self::$pages[$id]) ? self::$pages[$id] : self::$pages[0];
        }

        public static function getPages() {
            return self::$pages;
        }

        private static $pages2 = [
            0 => 'index',
            1 => 'buy',
            2 => 'main',
            3 => 'mining',
            4 => 'trade',
            5 => 'cards',
            6 => 'itemcard/{id}',
            7 => 'itemcard2/{id}',
            8 => 'blog',
            9 => 'about',
        ];

        public static function getPage2ById($id, $i_id = '') {
            return isset(self::$pages2[$id]) ? str_replace('{id}', $i_id, self::$pages2[$id]) : str_replace('{id}', $i_id, self::$pages2[0]);
        }

        public static function getPages2() {
            return self::$pages2;
        }

        const POS_MENU      = 0;
        const POS_START     = 1;
        const POS_BLOCK1    = 2;
        const POS_BLOCK2    = 3;
        const POS_BLOCK3    = 4;
        const POS_BLOCK4    = 5;
        const POS_BLOCK5    = 6;
        const POS_BLOCK6    = 7;
        const POS_BLOCK7    = 8;
        const POS_BLOCK8    = 9;
        const POS_BLOCK9    = 10;
        const POS_BLOCK10   = 11;
        const POS_BLOCK11   = 12;
        const POS_BLOCK12   = 13;
        const POS_END       = 14;
        const POS_MENU2     = 15;

        private static $positions = [
            0   => 'POS_MENU',
            1   => 'POS_START',
            2   => 'POS_BLOCK1',
            3   => 'POS_BLOCK2',
            4   => 'POS_BLOCK3',
            5   => 'POS_BLOCK4',
            6   => 'POS_BLOCK5',
            7   => 'POS_BLOCK6',
            8   => 'POS_BLOCK7',
            9   => 'POS_BLOCK8',
            10  => 'POS_BLOCK9',
            11  => 'POS_BLOCK10',
            12  => 'POS_BLOCK11',
            13  => 'POS_BLOCK12',
            14  => 'POS_END',
            15  => 'POS_MENU2',
        ];

        public static function getPositionById($id) {
            return isset(self::$positions[$id]) ? self::$positions[$id] : self::$positions[13];
        }

        public static function getPositions() {
            return self::$positions;
        }

        public $kun;
        public $oy;
        public $yil;
        public $max_bdate;
        
        public static function tableName() {

            return 'b_data';

        }

        public function rules() {

            return [

                [['bdate', 'commission', 'random_c', 'people', 'random_p', 'trader'], 'required'],

                [['bdate', 'created_at'], 'safe'],

                [['commission', 'random_c', 'page_id'], 'number'],

                [['people', 'random_p', 'trader'], 'integer'],

            ];

        }

        public function attributeLabels() {

            return [

                'id'        => 'ID',

                'bdate'     => 'Bdate',

                'commission'=> 'Commission',

                'random_c'  => 'Random C',

                'people'    => 'People',

                'random_p'  => 'Random P',

                'trader'    => 'Trader',

                'created_at'=> 'Created At',

                'page_id'   => 'Page',

            ];

        }

        public function findByPage($page = 0) {

            return self::find()->where(['page_id' => $page]);
            
        }

        public function getGraphic($page = 0) {

            $config = Yii::$app->params;

            $date_start = strtotime('2017-12-04');

            $now        = date('Y-m-d');

            $date_end   = strtotime($now);

            $page_name  = self::getPageById($page);

            if (isset($config['commission'][$page_name]) AND isset($config['peoples'][$page_name]) AND isset($config['trader'][$page_name])) {

                $from_c     = (int)($config['commission'][$page_name]['from'] * 1000);

                $to_c       = (int)($config['commission'][$page_name]['to'] * 1000);

                $from_p     = (int)$config['peoples'][$page_name]['from'];

                $to_p       = (int)$config['peoples'][$page_name]['to'];

                $from_t     = (int)$config['trader'][$page_name]['from'];

                $to_t       = (int)$config['trader'][$page_name]['to'];

                $commission = $people = 0;

                $interval   = 60*60*24;

                if ($res = $this->findByPage($page)->orderBy(['bdate' => SORT_DESC])->limit(1)->one()) {
                
                    $commission = (float)$res->commission;

                    $people     = (int)$res->people;

                    $date_start = (strtotime($res->bdate) + $interval);

                }

                if ($date_start <= $date_end) {

                    for ($i = $date_start; $i <= $date_end; $i += $interval) {

                        $random_c   = (rand($from_c, $to_c) / 1000);
                        $commission += $random_c;
                        $random_p   = rand($from_p, $to_p);
                        $people     += $random_p;
                        $trader     = rand($from_t, $to_t);
                        $model      = new self();

                        $model->bdate       = date('Y-m-d', $i);
                        $model->commission  = $commission;
                        $model->random_c    = $random_c;
                        $model->people      = $people;
                        $model->random_p    = $random_p;
                        $model->trader      = $trader;
                        $model->page_id     = $page;
                        $model->save();

                    }

                }

                $count  = $this->findByPage($page)->count();

                /******************************GRAPHIC1 && GRAPHIC2************/

                $model_q= $this->findByPage($page);

                if ($count > 365) {

                    $model_q->select([
                        "YEAR(bdate) yil",
                        "DATE_FORMAT(MAX(bdate), '%m') max_bdate",
                        "MAX(commission) commission",
                        "MAX(people) people",
                    ])->groupBy(["YEAR(bdate)"]);

                } else{

                    $model_q->select([
                        "DAY(bdate) kun",
                        "MONTH(bdate) oy",
                        "YEAR(bdate) yil",
                        "DATE_FORMAT(MAX(bdate), '%d') max_bdate",
                        "MAX(commission) commission",
                        "MAX(people) people",
                    ])->groupBy(['YEAR(bdate)', "MONTH(bdate)"]);

                }

                $data   = $model_q->orderBy(['bdate' => SORT_ASC])->all();

                $first          = isset($data[0]->oy) ? $data[0]->oy : $data[0]->yil;

                $second         = isset(end($data)->oy) ? end($data)->oy : end($data)->yil;

                $gLabels        = [];

                $gTitles        = [];

                $gContents1     = $gContents2 = [0];

                $max_bdate      = $max_label = '';

                foreach ($data as $key => $value) {

                    $max_bdate  = isset($value->oy) ? (int)$value->max_bdate : NHelper::tryGetMonthName((int)$value->max_bdate);

                    $max_label  = isset($value->oy) ? NHelper::tryGetMonthName($value->oy) : $value->yil;

                    $gLabels[]  = $max_label;

                    $gTitles[]  = (isset($value->oy) AND isset($value->kun)) ? $value->kun.' - '.NHelper::tryGetMonthName($value->oy) : '';

                    $gContents1[] = (float)$value->commission;

                    $gContents2[] = (int)$value->people;

                }

                $gLabels[]      = '';

                $gTitles[]      = $max_bdate.' - '.$max_label;

                $result['label']          = ($first == $second)?NHelper::tryGetMonthName($first):NHelper::tryGetMonthName($first).' - '.NHelper::tryGetMonthName($second);

                $result['gLabelsJson']    = json_encode($gLabels);

                $result['gtitleJson']     = json_encode($gTitles);

                $result['gContents1Json'] = json_encode($gContents1);

                $result['gContents2Json'] = json_encode($gContents2);
                
                return $result;
                
            }

            return ['label' => '', 'gLabelsJson' => json_encode([]), 'gtitleJson' => json_encode([]), 'gContents1Json' => json_encode([])];
            
        }

    }
