<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

class Admcards extends \yii\db\ActiveRecord {

    public $imageFile;
    public $imageFileLarge;

    public static function tableName() {

        return 'cards';
    }

    public function rules() {

        return [
            [['name_ru', 'name_en', 'name_ar', 'name_de', 'name_es', 'name_it'], 'required'],
            [['bonus_ru', 'bonus_en', 'bonus_ar', 'bonus_de', 'bonus_es', 'bonus_it', 'more_card_ru', 'more_card_en', 'more_card_ar', 'more_card_de', 'more_card_es', 'more_card_it', 'advant_limit_ru', 'advant_limit_en', 'advant_limit_ar', 'advant_limit_de', 'advant_limit_es', 'advant_limit_it', 'about_ru', 'about_en', 'about_ar', 'about_de', 'about_es', 'about_it', 'plus_minus_ru', 'plus_minus_en', 'plus_minus_ar', 'plus_minus_de', 'plus_minus_es', 'plus_minus_it', 'any_card_ru', 'any_card_en', 'any_card_ar', 'any_card_de', 'any_card_es', 'any_card_it'], 'string'],
            [['name_ru', 'name_en', 'name_ar', 'name_de', 'name_es', 'name_it', 'img_small', 'img_big', 'link'], 'string', 'max' => 250],
            [['status'], 'integer'],
            [['color'], 'string', 'max' => 50],
            [['created_at', 'updated_at'], 'safe'],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'on' => 'create'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'on' => 'update'],
            [['imageFileLarge'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'on' => 'create'],
            [['imageFileLarge'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'on' => 'update'],
        ];

    }

    public function attributeLabels() {

        return [
            'id' => 'ID',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'name_ar' => 'Name Ar',
            'name_de' => 'Name De',
            'name_es' => 'Name Es',
            'name_it' => 'Name It',
            'img_small' => 'Img Small',
            'img_big' => 'Img Big',
            'link' => 'Link',
            'bonus_ru' => 'Bonus Ru',
            'bonus_en' => 'Bonus En',
            'bonus_ar' => 'Bonus Ar',
            'bonus_de' => 'Bonus De',
            'bonus_es' => 'Bonus Es',
            'bonus_it' => 'Bonus It',
            'more_card_ru' => 'More Card Ru',
            'more_card_en' => 'More Card En',
            'more_card_ar' => 'More Card Ar',
            'more_card_de' => 'More Card De',
            'more_card_es' => 'More Card Es',
            'more_card_it' => 'More Card It',
            'advant_limit_ru' => 'Advant Limit Ru',
            'advant_limit_en' => 'Advant Limit En',
            'advant_limit_ar' => 'Advant Limit Ar',
            'advant_limit_de' => 'Advant Limit De',
            'advant_limit_es' => 'Advant Limit Es',
            'advant_limit_it' => 'Advant Limit It',
            'about_ru' => 'About Ru',
            'about_en' => 'About En',
            'about_ar' => 'About Ar',
            'about_de' => 'About De',
            'about_es' => 'About Es',
            'about_it' => 'About It',
            'plus_minus_ru' => 'Plus Minus Ru',
            'plus_minus_en' => 'Plus Minus En',
            'plus_minus_ar' => 'Plus Minus Ar',
            'plus_minus_de' => 'Plus Minus De',
            'plus_minus_es' => 'Plus Minus Es',
            'plus_minus_it' => 'Plus Minus It',
            'any_card_ru' => 'Any Card Ru',
            'any_card_en' => 'Any Card En',
            'any_card_ar' => 'Any Card Ar',
            'any_card_de' => 'Any Card De',
            'any_card_es' => 'Any Card Es',
            'any_card_it' => 'Any Card It',
            'created_at' => 'Created At',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'color' => 'Color',
        ];

    }

    public function beforeSave($insert) {

        if (parent::beforeSave($insert)) {
     
            if (!$this->isNewRecord) {

                $this->updated_at = date('Y-m-d H:i:s');

            }
     
            return true;
        }

        return false;

    }

    public function upload() {

        if ($this->validate()) {

            $path = 'uploads/cards';

            $name = $path . '/' . rand() . uniqid() . time() . '.' . $this->imageFile->extension;

            FileHelper::createDirectory($path);

            $this->imageFile->saveAs($name);

            if (!empty($this->img_small)) {

                @unlink($this->img_small);

            }

            $this->img_small = $name;

            return true;

        } else {

            return false;

        }

    }

    public function uploadLarge() {
            
        $path = 'uploads/cards';

        $name = $path . '/' . rand() . uniqid() . time() . '.' . $this->imageFileLarge->extension;

        FileHelper::createDirectory($path);


        $this->imageFileLarge->saveAs($name);

        if (!empty($this->img_big)) {

            @unlink($this->img_big);

        }

        $this->img_big = $name;

        return true;

    }

    public static function findByLan() {
        $page_list = self::find()->select(['id', 'name_en'])->where(['status' => 1])->all();
        $list = ArrayHelper::map($page_list, 'id', 'name_en');
        if (!empty($list)) {
            foreach ($list as $key => $value) {
                $list[$key] = strip_tags($value);
            }
        }
        return ['cards' => $list];
    }

    public function getName() {
        $lan = Yii::$app->language;
        $txt = 'name_'.$lan;
        return $this->$txt;
    }

    public function getBonus() {
        $lan = Yii::$app->language;
        $txt = 'bonus_'.$lan;
        return $this->$txt;
    }

    public function getMore_card() {
        $lan = Yii::$app->language;
        $txt = 'more_card_'.$lan;
        return $this->$txt;
    }

    public function getAdvant_limit() {
        $lan = Yii::$app->language;
        $txt = 'advant_limit_'.$lan;
        return $this->$txt;
    }

    public function getAbout() {
        $lan = Yii::$app->language;
        $txt = 'about_'.$lan;
        return $this->$txt;
    }

    public function getPlus_minus() {
        $lan = Yii::$app->language;
        $txt = 'plus_minus_'.$lan;
        return $this->$txt;
    }

    public function getAny_card() {
        $lan = Yii::$app->language;
        $txt = 'any_card_'.$lan;
        return $this->$txt;
    }

}
