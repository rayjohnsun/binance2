<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

class Users extends \yii\db\ActiveRecord implements IdentityInterface {

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    public static function tableName() {

        return 'users';

    }

    public function rules() {

        return [
            [['username', 'auth_key', 'password_hash', 'email'], 'required'],
            [['created_at', 'updated_at', 'password_reset_token'], 'safe'],
            [['username'], 'string', 'max' => 250],
            [['auth_key'], 'string', 'max' => 100],
            [['password_hash', 'password_reset_token', 'email', 'fio'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
            [['status'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['email'], 'unique'],
            [['username'], 'unique'],
        ];

    }

    public function attributeLabels() {

        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'fio' => 'Fio',
        ];

    }

    public static function findIdentity($id) {

        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);

    }

    public static function findIdentityByAccessToken($token, $type = null) {

        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');

    }

    public static function findByUsername($username) {

        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);

    }

    public static function findByEmail($email) {

        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);

    }

    public function getId() {

        return $this->getPrimaryKey();

    }

    public function getAuthKey() {

        return $this->auth_key;

    }

    public function validateAuthKey($authKey) {

        return $this->getAuthKey() === $authKey;

    }

    public function validatePassword($password) {

        return Yii::$app->security->validatePassword($password, $this->password_hash);

    }

    public function setPassword($password) {

        $this->password_hash = Yii::$app->security->generatePasswordHash($password);

    }

    public function generateAuthKey() {

        $this->auth_key = Yii::$app->security->generateRandomString();

    }

    public static function findByPasswordResetToken($token) {
     
        if (!static::isPasswordResetTokenValid($token)) {

            return null;

        }
     
        return static::findOne([

            'password_reset_token' => $token,

            'status' => self::STATUS_ACTIVE,

        ]);

    }
     
    public static function isPasswordResetTokenValid($token) {
     
        if (empty($token)) {

            return false;

        }
     
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);

        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();

    }
     
    public function generatePasswordResetToken() {

        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();

    }
     
    public function removePasswordResetToken() {

        $this->password_reset_token = null;

    }

}
