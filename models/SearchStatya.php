<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Statya;

/**
 * SearchStatya represents the model behind the search form of `app\models\Statya`.
 */
class SearchStatya extends Statya
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_id'], 'integer'],
            [['title_en', 'image', 'created_at', 'updated_at', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Statya::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'page_id' => $this->page_id,
        ]);

        $s_text = $this->created_at;
        if (strlen($this->created_at) > 10) {
            $s_text = date('Y-m-d', strtotime($this->created_at));
        }

        $t_text = $this->created_at;
        if (strlen($this->created_at) > 10) {
            $t_text = date('Y-m-d', strtotime($this->updated_at));
        }

        $query->andFilterWhere(['like', 'title_en', $this->title])
            ->andFilterWhere(['like', 'created_at', $s_text])
            ->andFilterWhere(['like', 'updated_at', $t_text])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
