<?php

namespace app\models;

use Yii;

class Pagedata extends \yii\db\ActiveRecord {

    public static function tableName() {

        return 'pagedata';

    }

    public function rules() {

        return [
            [['text_ru', 'text_en', 'text_ar', 'text_de', 'text_es', 'text_it'], 'string'],
            [['title_ru', 'title_en', 'title_ar', 'title_de', 'title_es', 'title_it'], 'string', 'max' => 250],
            [['status', 'page_id', 'position_id'], 'integer'],
            [['created_at'], 'safe'],
        ];

    }

    public function attributeLabels() {

        return [
            'id'            => 'ID',
            'title_ru'      => 'Title Ru',
            'title_en'      => 'Title En',
            'title_ar'      => 'Title Ar',
            'title_de'      => 'Title De',
            'title_es'      => 'Title Es',
            'title_it'      => 'Title It',
            'text_ru'       => 'Text Ru',
            'text_en'       => 'Text En',
            'text_ar'       => 'Text Ar',
            'text_de'       => 'Text De',
            'text_es'       => 'Text Es',
            'text_it'       => 'Text It',
            'status'        => 'Status',
            'created_at'    => 'Created At',
            'page_id'       => 'Page',
            'position_id'   => 'Position',
        ];

    }

    public function getText() {
        $lan = Yii::$app->language;
        $txt = 'text_'.$lan;
        return $this->$txt;
    }

    public static function byLanAndPage($p_id, $pos_id) {
        $q = self::find();
        $q->select(['text_'.Yii::$app->language]);
        $q->where(['status' => 1, 'page_id' => $p_id, 'position_id' => $pos_id]);
        $q->orderBy(['id' => SORT_ASC]);
        return $q;
    }

    public function textReplaced($replacements = []) {

        $txt = $this->text;

        if (!empty($replacements) AND is_array($replacements)) {
            
            foreach ($replacements as $key => $value) {

                $txt = str_replace($key, $value, $txt);
                
            }

        }

        return $txt;
        
    }

}
