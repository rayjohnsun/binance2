<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class Birja extends \yii\db\ActiveRecord {

    public $imageFile;
    public $sovpadenie;

    public static function tableName() {

        return 'birja';

    }

    public function rules() {

        return [
            [['name'], 'required'],
            [['plus', 'minus', 'short_text', 'full_text'], 'string'],
            [['name', 'icon', 'link'], 'string', 'max' => 250],
            [['name'], 'unique'],
            [['status'], 'integer'],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'on' => 'create'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'on' => 'update'],
            [['created_at', 'updated_at', 'coins'], 'safe'],
        ];

    }

    public function attributeLabels() {

        return [
            'id' => 'ID',
            'name' => 'Name',
            'icon' => 'Icon',
            'plus' => 'Plus',
            'minus' => 'Minus',
            'short_text' => 'Short Text',
            'full_text' => 'Full Text',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'coins' => 'Coins',
            'link' => 'Link',
        ];

    }

    public function upload() {

        if ($this->validate()) {

            $path = 'uploads/birja';

            $name = $path . '/' . rand() . uniqid() . time() . '.' . $this->imageFile->extension;

            FileHelper::createDirectory($path);

            $this->imageFile->saveAs($name);

            if (!empty($this->icon)) {

                unlink($this->icon);

            }

            $this->icon = $name;

            return true;

        } else {

            return false;

        }

    }

    public function beforeSave($insert) {

        if (parent::beforeSave($insert)) {
     
            if (!$this->isNewRecord) {

                $this->updated_at = date('Y-m-d H:i:s');

            }
     
            return true;
        }

        return false;

    }
    
}
