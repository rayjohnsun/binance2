<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Categories;

/**
 * SearchCategories represents the model behind the search form of `app\models\Categories`.
 */
class SearchCategories extends Categories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'norder', 'parent_id'], 'integer'],
            [['name_ru', 'name_en', 'name_ar', 'name_de', 'name_es', 'name_it', 'link', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Categories::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'        => $this->id,
            'status'    => $this->status,
            'norder'    => $this->norder,
            'parent_id' => $this->parent_id,
        ]);

        $s_text = $this->created_at;
        if (strlen($this->created_at) > 10) {
            $s_text = date('Y-m-d', strtotime($this->created_at));
        }

        $query->andFilterWhere(['like', 'name_ru', $this->name_ru])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'name_ar', $this->name_ar])
            ->andFilterWhere(['like', 'name_de', $this->name_de])
            ->andFilterWhere(['like', 'name_es', $this->name_es])
            ->andFilterWhere(['like', 'name_it', $this->name_it])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'created_at', $s_text]);

        return $dataProvider;
    }
}
