<?php

namespace app\models;

use Yii;

class Subscribers extends \yii\db\ActiveRecord {

    public static function tableName() {

        return 'subscribers';

    }

    public function rules() {

        return [
            [['imya', 'email'], 'required'],
            [['imya', 'email'], 'string', 'max' => 100],
            [['adres'], 'string', 'max' => 250],
            [['status'], 'integer'],
            [['created_at'], 'safe'],
        ];

    }

    public function attributeLabels() {

        return [
            'id'        => 'ID',
            'imya'      => 'Imya',
            'email'     => 'Email',
            'created_at'=> 'Created At',
            'status'    => 'Status',
            'adres'     => 'Address',
        ];
        
    }
}
