<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pagedata;

class SearchPagedata extends Pagedata {

    public function rules() {
        return [
            [['id', 'page_id', 'position_id'], 'integer'],
            [['title_en', 'status', 'created_at'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {

        $query = Pagedata::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;

        }

        $query->andFilterWhere([
            'id'            => $this->id,
            'page_id'       => $this->page_id,
            'position_id'   => $this->position_id,
            'status'        => $this->status,
        ]);

        $s_text = $this->created_at;
        if (strlen($this->created_at) > 10) {
            $s_text = date('Y-m-d', strtotime($this->created_at));
        }

        $query->andFilterWhere(['like', 'title_en', $this->title_en])
            ->andFilterWhere(['like', 'created_at', $s_text]);

        return $dataProvider;
    }
}
