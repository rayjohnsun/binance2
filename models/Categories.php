<?php

namespace app\models;

use Yii;

class Categories extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'categories';
    }

    public function rules() {
        return [
            [['name_ru', 'name_en', 'name_ar', 'name_de', 'name_es', 'name_it'], 'required'],
            [['name_ru', 'name_en', 'name_ar', 'name_de', 'name_es', 'name_it', 'link'], 'string', 'max' => 250],
            [['status', 'parent_id', 'norder', 'inline_with_next', 'top', 'bottom', 'page_id'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id'        => 'ID',
            'page_id'   => 'Page',
            'name_ru'   => 'Name Ru',
            'name_en'   => 'Name En',
            'name_ar'   => 'Name Ar',
            'name_de'   => 'Name De',
            'name_es'   => 'Name Es',
            'name_it'   => 'Name It',
            'link'      => 'Link',
            'status'    => 'Status',
            'created_at'=> 'Created At',
            'parent_id' => 'Parent ID',
            'norder'    => 'Order',
            'inline_with_next'    => 'Set inline with the next item',
            'top'       => 'Show in top menyu',
            'bottom'    => 'Show in bottom menyu',
        ];
    }

    public function getParents($position = '') {

        $array = [0 => 'Main'];
        $items = self::find()->where(['parent_id' => 0, 'status' => 1]);

        if ($position == 'top') {
            $items->andWhere(['top' => 1]);
        } else if($position == 'bottom') {
            $items->andWhere(['bottom' => 1]);
        }

        if ($models = $items->all()) {

            foreach ($models as $key => $value) {

                $array[$value->id] = $value->name_en;
                
            }
            
        }

        return $array;
    }

    public static function getMenyuItems($position = '') {

        $lan = Yii::$app->language;

        $menu = self::find();
        $menu->select(['id', 'name_'.$lan, 'link', 'inline_with_next']);
        $menu->where(['parent_id' => 0]);
        if ($position == 'top') {
            $menu->andWhere(['top' => 1]);
        } else if($position == 'bottom') {
            $menu->andWhere(['bottom' => 1]);
        }
        $menu->orderBy(['norder' => SORT_ASC]);

        return $menu->all();
        
    }

    public function getSubmenu() {

        return $this->hasMany(self::className(), ['parent_id' => 'id'])->onCondition(['status' => 1]);
        
    }

    // public function getSubmenu2() {

    //     return $this->hasMany(Admcards::className(), ['menu_id' => 'id'])->onCondition(['status' => 1]);
        
    // }

    public function getParent() {

        return $this->hasOne(self::className(), ['id' => 'parent_id']);
        
    }

    public function getName() {

        $lan = Yii::$app->language;

        $name = 'name_'.$lan;

        return $this->$name;
        
    }

    public function getSsilka() {
        $id = $this->page_id;
        $link = $this->link;
        $new_link = '#';
        if (!empty($link)) {
            $new_link = $link;
            if ($id > 0) {
                $new_link .= $id;
            }
        }
        return $new_link;
    }
}
