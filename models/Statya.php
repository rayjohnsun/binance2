<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;

class Statya extends \yii\db\ActiveRecord {

    public $imageFile = '';

    public static function tableName() {

        return 'statya';

    }

    public function rules() {

        return [
            [['title_ru', 'title_en', 'title_ar', 'title_de', 'title_es', 'title_it', 'short_text_ru', 'short_text_en', 'short_text_ar', 'short_text_de', 'short_text_es', 'short_text_it', 'page_id'], 'required'],
            [['full_text_ru', 'full_text_en', 'full_text_ar', 'full_text_de', 'full_text_es', 'full_text_it'], 'string'],
            [['title_ru', 'title_en', 'title_ar', 'title_de', 'title_es', 'title_it', 'short_text_ru', 'short_text_en', 'short_text_ar', 'short_text_de', 'short_text_es', 'short_text_it', 'image'], 'string', 'max' => 250],
            [['status', 'page_id'], 'integer'],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'on' => 'create'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'on' => 'update'],
            [['created_at', 'updated_at'], 'safe'],
        ];

    }

    public function attributeLabels() {

        return [
            'id' => 'ID',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'title_ar' => 'Title Ar',
            'title_de' => 'Title De',
            'title_es' => 'Title Es',
            'title_it' => 'Title It',
            'short_text_ru' => 'Short Text Ru',
            'short_text_en' => 'Short Text En',
            'short_text_ar' => 'Short Text Ar',
            'short_text_de' => 'Short Text De',
            'short_text_es' => 'Short Text Es',
            'short_text_it' => 'Short Text It',
            'full_text_ru' => 'Full Text Ru',
            'full_text_en' => 'Full Text En',
            'full_text_ar' => 'Full Text Ar',
            'full_text_de' => 'Full Text De',
            'full_text_es' => 'Full Text Es',
            'full_text_it' => 'Full Text It',
            'image' => 'Image',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'page_id' => 'Page',
        ];

    }

    public function upload() {

        if ($this->validate()) {

            $path = 'uploads/statya';

            $name = $path . '/' . rand() . uniqid() . time() . '.' . $this->imageFile->extension;

            FileHelper::createDirectory($path);

            $this->imageFile->saveAs($name);

            if (!empty($this->image)) {

                unlink($this->image);

            }

            $this->image = $name;

            return true;

        } else {

            return false;

        }

    }

    public function beforeSave($insert) {

        if (parent::beforeSave($insert)) {

            if (!isset(Bdata::getPages()[$this->page_id])) {

                $this->page_id = Bdata::PAGE_INDEX;
                
            }
     
            if (!$this->isNewRecord) {

                $this->updated_at = date('Y-m-d H:i:s');

            } else {

                $this->created_at = date('Y-m-d H:i:s');

            }
     
            return true;
        }

        return false;

    }

    public function getTitle() {
        $lan = Yii::$app->language;
        $txt = 'title_'.$lan;
        return $this->$txt;
    }

    public function getShort_text() {
        $lan = Yii::$app->language;
        $txt = 'short_text_'.$lan;
        return $this->$txt;
    }

    public function getFull_text() {
        $lan = Yii::$app->language;
        $txt = 'full_text_'.$lan;
        return $this->$txt;
    }
    
}
