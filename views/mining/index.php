<?php 

	use app\models\Bdata;

?>
<section id="about" class="about">

	<?php if (!empty($block1)): ?>

		<div class="container">
			
			<?=$block1->text ?>

		</div>

	<?php endif ?>

</section>

<section class="graph">

    <div class="container">

        <canvas id="myChart" width="200"></canvas>

    </div>

</section>

<section id="plus" class="plus">

	<ins class="line-1"></ins><ins class="line-2"></ins><ins class="line-3"></ins>

	<?php if (!empty($block2)): ?>
		
		<div class="container">

			<?=$block2->text ?>

		</div>

	<?php endif ?>

</section>

<section id="news" class="block bg_blue">

	<div class="container">

		<div class="row">

			<h2 class="text-center"><?=t('Last news') ?><br /><?=t('and prices crypto currency') ?></h2>
			
			<div class="col-md-6 col-sm-12 news news-js">&nbsp;</div>
				
			<div class="col-md-6 col-sm-12">
				
				<div class="currency">

					<div class="currency-table">

						<div class="th">

							<div style="width: 49%;"><?=t('Crypto currency') ?> <span><?=t('Name2') ?></span></div>
							
							<div style="width: 19%;"><?=t('Price') ?> <span>USD</span></div>
							
							<div style="width: 29%;"><?=t('Changes') ?> <span><?=t('in 24 hours') ?></span></div>

						</div>
						
						<div class="currency-js">&nbsp;</div>
					
					</div>
				
				</div>
				
			</div>
			
		</div>

	</div>

</section>

<?=$this->render('/layouts/_obzor_stati', ['p_id' => Bdata::PAGE_MINING]); ?>

<script>

	$(document).ready(function(){

		loadCoins();

		loadNewsTo('.news-js');

		var label = '<?=$gdata["label"] ?>';
		var labels= JSON.parse('<?=$gdata["gLabelsJson"] ?>');
		var title = JSON.parse('<?=$gdata["gtitleJson"] ?>');
		var comessions = JSON.parse('<?=$gdata["gContents1Json"] ?>');
		createGraphic1('myChart', label, labels, title, comessions);

	});

</script>