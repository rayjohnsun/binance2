<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Admcards */

$this->title = 'Create Admcards';
$this->params['breadcrumbs'][] = ['label' => 'Admcards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admcards-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
