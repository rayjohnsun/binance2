<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Admcards */

$this->title = strip_tags($model->name);
$this->params['breadcrumbs'][] = ['label' => 'Admcards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admcards-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name_en',
            'img_small',
            'img_big',
            'link',
            'bonus_en:ntext',
            'advant_limit_en:ntext',
            'about_en:ntext',
            'created_at',
            'status',
            'updated_at',
        ],
    ]) ?>

</div>
