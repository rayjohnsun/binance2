<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Admcards */

$this->title = 'Update Admcards: ' . strip_tags($model->name);
$this->params['breadcrumbs'][] = ['label' => 'Admcards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => strip_tags($model->name), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="admcards-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
