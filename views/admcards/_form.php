<?php

    use conquer\codemirror\CodemirrorAsset;
    use conquer\codemirror\CodemirrorWidget;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $langs = Yii::$app->params['languages'];

?>

<div class="admcards-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form->field($model, 'imageFileLarge')->fileInput() ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <ul class="nav nav-tabs" id="NavTabs">

        <?php $a = 0; foreach ($langs as $key => $value): ?>
            
            <li class="<?=$a==0?'active':'' ?>">

                <a href="#<?=$key.'_'.$value['id'] ?>"><?=$value['name'] ?></a>

            </li>

        <?php $a++; endforeach ?>

    </ul>

    <div class="tab-content">

        <?php $a = 0; foreach ($langs as $key => $value): ?>

            <div id="<?=$key.'_'.$value['id'] ?>" class="tab-pane2 <?=$a==0?'active2':'' ?>">
                
                <?= $form->field($model, 'name_'.$key)->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'bonus_'.$key)->widget(CodemirrorWidget::className(), [
                    'preset' =>'php',
                    'options'=>['rows' => 20, 'id' => 'bonus_area_'.$key],
                    'assets'=>[
                        CodemirrorAsset::THEME_MONOKAI,
                        CodemirrorAsset::KEYMAP_SUBLIME,
                        CodemirrorAsset::MODE_XML,
                    ],
                    'settings' => [
                        'theme' => "monokai",
                        'mode' => 'xml',
                        'keyMap' => 'sublime',
                        'lineNumbers' => true,
                        'styleActiveLine' => true,
                        'matchBrackets' => true,
                        'showCursorWhenSelecting' => true,
                        'tabSize' => 2,
                        'htmlMode' => true,
                        'autoCloseBrackets' => true,
                    ]
                ]) ?>

            </div>

        <?php $a++; endforeach ?>

    </div>

    <ul class="nav nav-tabs" id="NavTabs2">

        <?php $a = 0; foreach ($langs as $key => $value): $key2 = $key.'2'; ?>
            
            <li class="<?=$a==0?'active':'' ?>">

                <a href="#<?=$key2.'_'.$value['id'] ?>"><?=$value['name'] ?></a>

            </li>

        <?php $a++; endforeach ?>

    </ul>

    <div class="tab-content">

        <?php $a = 0; foreach ($langs as $key => $value): $key2 = $key.'2'; ?>

            <div id="<?=$key2.'_'.$value['id'] ?>" class="tab-pane2 <?=$a==0?'active2':'' ?>">

                <?= $form->field($model, 'more_card_'.$key)->widget(CodemirrorWidget::className(), [
                    'preset' =>'php',
                    'options'=>['rows' => 20, 'id' => 'more_card_area_'.$key2],
                    'assets'=>[
                        CodemirrorAsset::THEME_MONOKAI,
                        CodemirrorAsset::KEYMAP_SUBLIME,
                        CodemirrorAsset::MODE_XML,
                    ],
                    'settings' => [
                        'theme' => "monokai",
                        'mode' => 'xml',
                        'keyMap' => 'sublime',
                        'lineNumbers' => true,
                        'styleActiveLine' => true,
                        'matchBrackets' => true,
                        'showCursorWhenSelecting' => true,
                        'tabSize' => 2,
                        'htmlMode' => true,
                        'autoCloseBrackets' => true,
                    ]
                ]) ?>

            </div>

        <?php $a++; endforeach ?>

    </div>

    <ul class="nav nav-tabs" id="NavTabs3">

        <?php $a = 0; foreach ($langs as $key => $value): $key2 = $key.'3'; ?>
            
            <li class="<?=$a==0?'active':'' ?>">

                <a href="#<?=$key2.'_'.$value['id'] ?>"><?=$value['name'] ?></a>

            </li>

        <?php $a++; endforeach ?>

    </ul>

    <div class="tab-content">

        <?php $a = 0; foreach ($langs as $key => $value): $key2 = $key.'3'; ?>

            <div id="<?=$key2.'_'.$value['id'] ?>" class="tab-pane2 <?=$a==0?'active2':'' ?>">

                <?= $form->field($model, 'advant_limit_'.$key)->widget(CodemirrorWidget::className(), [
                    'preset' =>'php',
                    'options'=>['rows' => 20, 'id' => 'advant_limit_area_'.$key2],
                    'assets'=>[
                        CodemirrorAsset::THEME_MONOKAI,
                        CodemirrorAsset::KEYMAP_SUBLIME,
                        CodemirrorAsset::MODE_XML,
                    ],
                    'settings' => [
                        'theme' => "monokai",
                        'mode' => 'xml',
                        'keyMap' => 'sublime',
                        'lineNumbers' => true,
                        'styleActiveLine' => true,
                        'matchBrackets' => true,
                        'showCursorWhenSelecting' => true,
                        'tabSize' => 2,
                        'htmlMode' => true,
                        'autoCloseBrackets' => true,
                    ]
                ]) ?>

            </div>

        <?php $a++; endforeach ?>

    </div>

    <ul class="nav nav-tabs" id="NavTabs4">

        <?php $a = 0; foreach ($langs as $key => $value): $key2 = $key.'4'; ?>
            
            <li class="<?=$a==0?'active':'' ?>">

                <a href="#<?=$key2.'_'.$value['id'] ?>"><?=$value['name'] ?></a>

            </li>

        <?php $a++; endforeach ?>

    </ul>

    <div class="tab-content">

        <?php $a = 0; foreach ($langs as $key => $value): $key2 = $key.'4'; ?>

            <div id="<?=$key2.'_'.$value['id'] ?>" class="tab-pane2 <?=$a==0?'active2':'' ?>">
                
                <?= $form->field($model, 'about_'.$key)->widget(CodemirrorWidget::className(), [
                    'preset' =>'php',
                    'options'=>['rows' => 20, 'id' => 'about_area_'.$key2],
                    'assets'=>[
                        CodemirrorAsset::THEME_MONOKAI,
                        CodemirrorAsset::KEYMAP_SUBLIME,
                        CodemirrorAsset::MODE_XML,
                    ],
                    'settings' => [
                        'theme' => "monokai",
                        'mode' => 'xml',
                        'keyMap' => 'sublime',
                        'lineNumbers' => true,
                        'styleActiveLine' => true,
                        'matchBrackets' => true,
                        'showCursorWhenSelecting' => true,
                        'tabSize' => 2,
                        'htmlMode' => true,
                        'autoCloseBrackets' => true,
                    ]
                ]) ?>

            </div>

        <?php $a++; endforeach ?>

    </div>

    <ul class="nav nav-tabs" id="NavTabs5">

        <?php $a = 0; foreach ($langs as $key => $value): $key2 = $key.'5'; ?>
            
            <li class="<?=$a==0?'active':'' ?>">

                <a href="#<?=$key2.'_'.$value['id'] ?>"><?=$value['name'] ?></a>

            </li>

        <?php $a++; endforeach ?>

    </ul>

    <div class="tab-content">

        <?php $a = 0; foreach ($langs as $key => $value): $key2 = $key.'5'; ?>

            <div id="<?=$key2.'_'.$value['id'] ?>" class="tab-pane2 <?=$a==0?'active2':'' ?>">
                
                <?= $form->field($model, 'plus_minus_'.$key)->widget(CodemirrorWidget::className(), [
                    'preset' =>'php',
                    'options'=>['rows' => 20, 'id' => 'plus_minus_area_'.$key2],
                    'assets'=>[
                        CodemirrorAsset::THEME_MONOKAI,
                        CodemirrorAsset::KEYMAP_SUBLIME,
                        CodemirrorAsset::MODE_XML,
                    ],
                    'settings' => [
                        'theme' => "monokai",
                        'mode' => 'xml',
                        'keyMap' => 'sublime',
                        'lineNumbers' => true,
                        'styleActiveLine' => true,
                        'matchBrackets' => true,
                        'showCursorWhenSelecting' => true,
                        'tabSize' => 2,
                        'htmlMode' => true,
                        'autoCloseBrackets' => true,
                    ]
                ]) ?>

            </div>

        <?php $a++; endforeach ?>

    </div>

    <ul class="nav nav-tabs" id="NavTabs6">

        <?php $a = 0; foreach ($langs as $key => $value): $key2 = $key.'6'; ?>
            
            <li class="<?=$a==0?'active':'' ?>">

                <a href="#<?=$key2.'_'.$value['id'] ?>"><?=$value['name'] ?></a>

            </li>

        <?php $a++; endforeach ?>

    </ul>

    <div class="tab-content">

        <?php $a = 0; foreach ($langs as $key => $value): $key2 = $key.'6'; ?>

            <div id="<?=$key2.'_'.$value['id'] ?>" class="tab-pane2 <?=$a==0?'active2':'' ?>">
                
                <?= $form->field($model, 'any_card_'.$key)->widget(CodemirrorWidget::className(), [
                    'preset' =>'php',
                    'options'=>['rows' => 20, 'id' => 'any_card_area_'.$key2],
                    'assets'=>[
                        CodemirrorAsset::THEME_MONOKAI,
                        CodemirrorAsset::KEYMAP_SUBLIME,
                        CodemirrorAsset::MODE_XML,
                    ],
                    'settings' => [
                        'theme' => "monokai",
                        'mode' => 'xml',
                        'keyMap' => 'sublime',
                        'lineNumbers' => true,
                        'styleActiveLine' => true,
                        'matchBrackets' => true,
                        'showCursorWhenSelecting' => true,
                        'tabSize' => 2,
                        'htmlMode' => true,
                        'autoCloseBrackets' => true,
                    ]
                ]) ?>

            </div>

        <?php $a++; endforeach ?>

    </div>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs('

    $(".tab-content .tab-pane2").each(function(){
        $(this).addClass("item2");
    });

    $("#NavTabs li a").click(function(e){

        e.preventDefault();

        aaa($(this));

    });
    
    $("#NavTabs2 li a").click(function(e){
        
        e.preventDefault();

        aaa($(this));

    });
    
    $("#NavTabs3 li a").click(function(e){
        
        e.preventDefault();

        aaa($(this));

    });
    
    $("#NavTabs4 li a").click(function(e){
        
        e.preventDefault();

        aaa($(this));

    });
    
    $("#NavTabs5 li a").click(function(e){
        
        e.preventDefault();

        aaa($(this));

    });
    
    $("#NavTabs6 li a").click(function(e){
        
        e.preventDefault();

        aaa($(this));

    });

    function aaa (t){

        t.parent().addClass("active");
        t.parent().siblings().removeClass("active");

        var b_id = t.attr("href");

        if($(b_id).length > 0){

            $(b_id).addClass("active2");
            $(b_id).siblings().removeClass("active2");

        }
        
    }

') ?>