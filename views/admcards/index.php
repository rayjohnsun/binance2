<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchAdmcards */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admcards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admcards-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Admcards', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'img_small',
                'header' => 'Image',
                'value' => function ($m) {
                    return '<img src="/'.$m->img_small.'" height="20">';
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'color',
                'value' => function ($m) {
                    return !empty($m->color) ? '<span class="label" style="background:'.$m->color.';">'.$m->color.'</span>' : '<code>None</code>';
                },
                'format'=> 'raw',
            ],
            'id',
            [
                'attribute' => 'name_en',
                'format'=> 'raw',
            ],
            'link',
            //'bonus:ntext',
            //'more_card:ntext',
            //'advant_limit:ntext',
            //'about:ntext',
            [
                'attribute' => 'status',
                'value' => function ($m) {
                    return $m->status > 0 ? '<span class="label label-success">Активный</span>' : '<span class="label label-danger">Не активный</span>';
                },
                'format'=> 'raw',
                'filter'=> Html::activeDropDownList($searchModel, 'status', [1 => 'Active', 0 => 'No active'],['class'=>'form-control','prompt' => '']),
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($m) {
                    $dt = date('d.m.Y H:i', strtotime($m->created_at));
                    return $dt;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
