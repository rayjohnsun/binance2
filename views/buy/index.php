<?php 

	use app\models\Bdata;

?>
<?php if (!empty($block1)): ?>
	<section id="about" class="about">
		<div class="container">
			<?=$block1->text ?>
		</div>
	</section>
<?php endif ?>

<section class="graph">

    <div class="container">

        <canvas id="myChart" width="200"></canvas>

    </div>

</section>

<section id="plus" class="block exchanges">
	<div class="container">
		<div class="row">
			<h2 class="text-center"><?=t('Exchange selection') ?></h2>
			<div class="col-sm-12 col-md-12">
				<div class="top"><p><?=t('Choose payment methods for crypto currency?') ?></p>
				
					<form action="" method="post" class="search white ui-widget">
						<input type="text"  class="form-control" name="search" id="search-exchanges" placeholder="Поиск..." maxlength="20">
						<input type="button" class="search-btn" value="" id="search-exchanges-btn" />
						
						<ul class="search-results" style="display: none;">
							<li><a href="link"><img src="images/main/icon-btn-small.png" width="15"> Bitcoin</a></li>
							<li><a href="link"><img src="images/main/icon-btn-small.png" width="15"> Bitcoin</a></li>
							<li><a href="link"><img src="images/main/icon-btn-small.png" width="15"> Bitcoin</a></li>
						</ul>
						
					</form>
				
				</div>
				
				<ul id="BirjaContent" class="birja_content">&nbsp;</ul>
				
			</div>
		</div>
	</div>
</section>


<section id="news" class="block bg_blue">
	
	<div class="container">
		
		<div class="row">
			
			<h2 class="text-center"><?=t('Last news') ?><br /><?=t('and prices crypto currency') ?></h2>
			
			<div class="col-md-6 col-sm-12 news news-js">&nbsp;</div>
			
			<div class="col-md-6 col-sm-12">
				
				<div class="currency">
					
					<div class="currency-table">
						
						<div class="th">
							
							<div style="width: 49%;"><?=t('Crypto currency') ?> <span><?=t('Name2') ?></span></div>
							
							<div style="width: 19%;"><?=t('Price') ?> <span>USD</span></div>
							
							<div style="width: 29%;"><?=t('Changes') ?> <span><?=t('in 24 hours') ?></span></div>
							
						</div>
						
						<div class="currency-js">&nbsp;</div>
					
					</div>
					
				</div>
				
				
			</div>
			
		</div>
		
	</div>
	
</section>

<?=$this->render('/layouts/_obzor_stati', ['p_id' => Bdata::PAGE_BUY]); ?>

<script>

	$(document).ready(function(){

		loadCoins();

		loadBirja('');

		$('#search-exchanges').keyup(function () {
			var val = $(this).val();
			loadBirja(val);
		});

		$('#search-exchanges-btn').click(function () {
			var val = $('#search-exchanges').val();
			loadBirja(val);
		});

		loadNewsTo('.news-js');

		var label = '<?=$gdata["label"] ?>';
		var labels= JSON.parse('<?=$gdata["gLabelsJson"] ?>');
		var title = JSON.parse('<?=$gdata["gtitleJson"] ?>');
		var comessions = JSON.parse('<?=$gdata["gContents1Json"] ?>');
		createGraphic1('myChart', label, labels, title, comessions);

	});

</script>