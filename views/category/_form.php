<?php

use app\models\Categories;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$langs = Yii::$app->params['languages'];

?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=$form->field($model, 'parent_id')->dropDownlist($model->parents) ?>

    <ul class="nav nav-tabs">

      <?php $a = 0; foreach ($langs as $key => $value): ?>
            
        <li class="<?=$a==0?'active':'' ?>">

          <a data-toggle="tab" href="#key_<?=$key ?>"><?=$value['name'] ?></a>

        </li>

      <?php $a++; endforeach ?>

    </ul>

    <div class="tab-content">

      <?php $a = 0; foreach ($langs as $key => $value): ?>

      <div class="tab-pane <?=$a==0?'in active':'' ?>" id="<?='key_'.$key ?>">

        <?=$form->field($model, 'name_'.$key)->textInput(['maxlength' => true]) ?>
        
      </div>

      <?php $a++; endforeach ?>
      
    </div>

    <?=$form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model, 'page_id')->dropDownlist($page_list, ['prompt' => '']) ?>

    <?=$form->field($model, 'status')->checkbox() ?>

    <?=$form->field($model, 'norder')->dropDownlist([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50]) ?>

    <?=$form->field($model, 'top')->checkbox() ?>

    <?=$form->field($model, 'bottom')->checkbox() ?>
    
    <?=$form->field($model, 'inline_with_next')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
