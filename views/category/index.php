<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchCategories */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Categories', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name_en',
            [
                'attribute' => 'link',
                'value' => function ($m) {
                    return $m->link . ($m->page_id > 0 ? $m->page_id : '');
                }
            ],
            'status',
            'norder',
            [
                'attribute' => 'parent_id',
                'value' => function ($m) {
                    if ($m->parent_id > 0) {
                        return $m->parent->name_en;
                    } else {
                        return '';
                    }
                },
                'filter'=> Html::activeDropDownList($searchModel, 'parent_id', $mod,['class'=>'form-control','prompt' => '']),
                'format' => 'raw',
            ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
