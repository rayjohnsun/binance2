<?php 

use app\models\Bdata;


?>
<section>
	<div class="container">
		<div class="bx-breadcrumb">
			<a href="/"><?=t('Home') ?></a>&raquo; <a href="/blog"><?=t('Blog') ?></a>&raquo; <span><?=$model->title ?></span>
		</div>
	</div>
</section>

<section id="text" class="block text-blog-item" style="padding-top: 0px;">
	<div class="container">
		<h1><?=$model->title ?></h1>
		<?=$model->full_text ?>
	</div>
</section>

<?=$this->render('/layouts/_obzor_stati2', ['p_id' => Bdata::PAGE_BLOG]); ?>

