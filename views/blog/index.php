<?php 

	use app\components\NHelper;

?>
<section>
	<div class="container">
		<div class="bx-breadcrumb">
			<a href="/"><?=t('Home') ?></a>&raquo; <span><?=t('Blog') ?></span>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
		
			<div class="col-sm-12 col-md-4">
				<div class="blog-category">
					<a href="link">Покупка криптовалюты за фиат <ins>(123)</ins></a>
				</div>
			</div>
			<div class="col-sm-12 col-md-4">
				<div class="blog-category">
					<a href="link">Как трейдить криптовалютные пары <ins>(123)</ins></a>
				</div>
			</div>
			<div class="col-sm-12 col-md-4">
				<div class="blog-category">
					<a href="link">Как трейдить криптовалютные пары <ins>(123)</ins></a>
				</div>
			</div>
			<div class="col-sm-12 col-md-4">
				<div class="blog-category">
					<a href="link">Холодные кошельки <ins>(123)</ins></a>
				</div>
			</div>
			<div class="col-sm-12 col-md-4">
				<div class="blog-category active">
					<a href="link">Другое <ins>(123)</ins></a>
				</div>
			</div>
		
		</div>
	</div>
</section>

<section id="articles" class="block articles">
	<div class="container">
		<div class="row">

			<?php if (!empty($models)): ?>

				<?php foreach ($models as $key => $value): ?>

					<div class="col-sm-12 col-md-4">
						<article class="item">
							<a href="/blog-item/<?=$value->id ?>">
								<?php if (!empty($value->image)): ?>
									<img src="/<?=$value->image ?>" />
								<?php endif ?>
							</a>
							<p><a href="/blog-item/<?=$value->id ?>"><?=$value->title ?></a></p>
							<span><a href="/blog-item/<?=$value->id ?>"><?=$value->short_text ?></a></span>
							<a href="/blog-item/<?=$value->id ?>"><?=t('Read more ...') ?></a>
							<ins><?=NHelper::vremya(!empty($value->updated_at) ? $value->updated_at : $value->created_at); ?></ins>
						</article>
					</div>
					
				<?php endforeach ?>
				
			<?php endif ?>

		</div>
	</div>
</section>

<?php if (!empty($pagenation)): ?>

	<section>
		<div class="container">
			<div class="pager">
				<?php foreach ($pagenation as $key2 => $value2): ?>

					<?php if ($value2['active'] == true): ?>

						<span><?=$value2['title'] ?></span>
						
					<?php else: ?>

						<?php if (!empty($value2['link'])): ?>
							
							<a href="<?=$value2['link'] ?>"><?=$value2['title'] ?></a>

						<?php else: ?>

							<a><?=$value2['title'] ?></a>
						
						<?php endif ?>

					<?php endif ?>
					
				<?php endforeach ?>
				<!-- <a href="link">1</a>
				<a href="link">2</a>
				<span>3</span>
				<a>...</a>
				<a href="link">22</a> -->
			</div>
		</div>
	</section>
	
<?php endif ?>
