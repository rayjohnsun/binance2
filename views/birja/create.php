<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Birja */

$this->title = 'Create Birja';
$this->params['breadcrumbs'][] = ['label' => 'Birja', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="birja-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
