<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchBirja */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Birja';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="birja-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Birja', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'icon',
                'header' => 'Image',
                'value' => function ($m) {
                    return '<img src="/'.$m->icon.'" height="20">';
                },
                'format' => 'raw',
            ],
            'id',
            'name',
            'plus:ntext',
            'minus:ntext',
            'short_text:ntext',
            //'full_text:ntext',
            [
                'attribute' => 'created_at',
                'value' => function ($m) {
                    $dt = date('d.m.Y H:i', strtotime($m->created_at));
                    return $dt;
                }
            ],
            //'updated_at',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
