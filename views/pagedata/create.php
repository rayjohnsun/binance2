<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pagedata */

$this->title = 'Create Pagedata';
$this->params['breadcrumbs'][] = ['label' => 'Page data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pagedata-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
