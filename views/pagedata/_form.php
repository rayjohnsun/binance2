<?php

use app\models\Bdata;
use conquer\codemirror\CodemirrorAsset;
use conquer\codemirror\CodemirrorWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$langs = Yii::$app->params['languages'];

?>

<div class="pagedata-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'page_id')->dropdownList(Bdata::getPages()) ?>

    <?= $form->field($model, 'position_id')->dropdownList(Bdata::getPositions()) ?>

    <ul class="nav nav-tabs" id="NavTabs">

        <?php $a = 0; foreach ($langs as $key => $value): ?>
            
            <li class="<?=$a==0?'active':'' ?>">

                <a href="#<?=$key.'_'.$value['id'] ?>"><?=$value['name'] ?></a>

            </li>

        <?php $a++; endforeach ?>

    </ul>

    <div class="tab-content">

        <?php $a = 0; foreach ($langs as $key => $value): ?>

            <div id="<?=$key.'_'.$value['id'] ?>" class="tab-pane2 <?=$a==0?'active2':'' ?>">

                <?= $form->field($model, 'title_'.$key)->textInput(['maxlength' => true]) ?>
                
                <?= $form->field($model, 'text_'.$key)->widget(CodemirrorWidget::className(), [
                    'preset' =>'php',
                    'options'=>['rows' => 20, 'id' => 'code_mirr_'.$key],
                    'assets'=>[
                        CodemirrorAsset::THEME_MONOKAI,
                        CodemirrorAsset::KEYMAP_SUBLIME,
                        CodemirrorAsset::MODE_XML,
                    ],
                    'settings' => [
                        'theme' => "monokai",
                        'mode' => 'xml',
                        'keyMap' => 'sublime',
                        'lineNumbers' => true,
                        'styleActiveLine' => true,
                        'matchBrackets' => true,
                        'showCursorWhenSelecting' => true,
                        'tabSize' => 2,
                        'htmlMode' => true,
                        'autoCloseBrackets' => true,
                    ]
                ]) ?>
                
            </div>

        <?php $a++; endforeach ?>

    </div>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs('

    $(".tab-content .tab-pane2").each(function(){
        $(this).addClass("item2");
    });

    $("#NavTabs li a").click(function(e){

        e.preventDefault();

        $(this).parent().addClass("active");
        $(this).parent().siblings().removeClass("active");

        var b_id = $(this).attr("href");

        if($(b_id).length > 0){

            $(b_id).addClass("active2");
            $(b_id).siblings().removeClass("active2");

        }

    });

') ?>
