<?php

use app\models\Bdata;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Page data';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="pagedata-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Pagedata', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'page_id',
                'value' => function ($m) {
                    return Bdata::getPageById($m->page_id);
                },
                'filter'=> Html::activeDropDownList($searchModel, 'page_id', Bdata::getPages(),['class'=>'form-control','prompt' => '']),
                'format' => 'raw',
            ],
            [
                'attribute' => 'position_id',
                'value' => function ($m) {
                    return Bdata::getPositionById($m->position_id);
                },
                'filter'=> Html::activeDropDownList($searchModel, 'position_id', Bdata::getPositions(),['class'=>'form-control','prompt' => '']),
                'format' => 'raw',
            ],
            'title_en',
            // 'title_en',
            // 'title_ar',
            // 'title_de',
            //'title_es',
            //'title_it',
            // 'text_ru:ntext',
            //'text_en:ntext',
            //'text_ar:ntext',
            //'text_de:ntext',
            //'text_es:ntext',
            //'text_it:ntext',
            [
                'attribute' => 'status',
                'value' => function ($m) {
                    return $m->status > 0 ? '<span class="label label-success">Активный</span>' : '<span class="label label-danger">Не активный</span>';
                },
                'format'=> 'raw',
                'filter'=> Html::activeDropDownList($searchModel, 'status', [1 => 'Active', 0 => 'No active'],['class'=>'form-control','prompt' => '']),
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($m) {
                    $dt = date('d.m.Y H:i', strtotime($m->created_at));
                    return $dt;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
