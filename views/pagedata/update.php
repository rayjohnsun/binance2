<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pagedata */

$this->title = 'Update Pagedata - ID:'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Page data', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pagedata-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
