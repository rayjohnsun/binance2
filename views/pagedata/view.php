<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pagedata */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Page data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pagedata-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title_ru',
            'title_en',
            'title_ar',
            'title_de',
            'title_es',
            'title_it',
            'text_ru:ntext',
            'text_en:ntext',
            'text_ar:ntext',
            'text_de:ntext',
            'text_es:ntext',
            'text_it:ntext',
            'status',
            'created_at',
            'page_id',
        ],
    ]) ?>

</div>
