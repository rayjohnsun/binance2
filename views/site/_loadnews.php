<?php 

	use app\components\NHelper;

?>
<?php if (!empty($nat)): ?>

	<?php foreach ($nat as $key => $value): ?>

		<?php $tdate = date("Y-m-d H:i:s", $value['published_on']) ?>

		<article>
			<div class="metka">
				<img src="<?=$value['imageurl'] ?>" alt="<?=$value['source'] ?>">
			</div>
			<p><?=$value['title'] ?></p>
			<span><?=$value['body'] ?></span>
			<a href="<?=$value['url'] ?>" target="_blank"><?=t('Read more ...') ?></a>
			<ins><?=NHelper::vremya($tdate) ?></ins>
		</article>
		
	<?php endforeach ?>
	
<?php endif ?>