<?php

    $this->title = 'Buy cryptocurrency on the stock exchange Binance at a discount | Referral link Binance';

?>

<header>

    <div class="container">

        <div class="navbar-header">

            <?php if (!empty($block_menu)): ?>

                <?=$block_menu->text ?>

            <?php endif ?>

        </div>

        <div class="row">

            <div class="col-md-7 col-sm-12">

                <?php if (!empty($block_start)): ?>
                    
                    <?=$block_start->text ?>
                
                <?php endif ?>

            </div>

            <div class="form-block">

                <form class="form" method="post" name="creatform">

                    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                    <!--<a href="link">link to another site area</a>-->

                    <input class="form-control" name="Message[subject]" id="subject" placeholder="<?=t('Binance Login') ?>" type="text">

                    <input class="form-control" name="Message[text]" id="text" placeholder="<?=t('Ethereum wallet adress') ?>" type="text">

                    <input class="form-control" name="Message[email]" id="email" placeholder="Email" type="text">

                    <button type="submit"><?=t('Send') ?></button>

                </form>

            </div>

        </div>

    </div>

</header>

<a name="plus"></a>

<?php if (!empty($block_1)): ?>

    <section class="block plus">

        <div class="container">

            <?=$block_1->text ?>

        </div>

    </section>

<?php endif ?>

<?php if (!empty($block_2)): ?>

    <section class="width-block link">

        <div class="container">

            <?=$block_2->text ?>

        </div>

    </section>

<?php endif ?>

<section class="graph">

    <div class="container">

        <canvas id="myChart" width="200"></canvas>

    </div>

</section>

<a name="exchange"></a>

<?php if (!empty($block_3)): ?>
    
    <section class="ul">

        <div class="container">

            <?=$block_3->text ?>

        </div>

    </section>

<?php endif ?>

<a name="bitcoin"></a>

<section class="carousel">

    <div class="container">

        <?php if (!empty($block_4)): ?>

            <?=$block_4->textReplaced([
                '{course}' => $apl1,
                '{summary}' => $apl2,
                '{trader}' => $active_trader
            ]) ?>

        <?php endif ?>

        <div class="row">

            <div class="col-md-12 col-sm-12" style="padding: 0px 50px;">

                <div class="owl-carousel owl-theme">

                    <?php foreach ($currencies as $k => $v): ?>

                        <div class="item">

                            <p><?=$v['from'] ?>/<?=$v['to'] ?></p>

                            <span><?=$v['price'] ?></span>

                            <span><?=$v['price'] ?></span>

                        </div>

                    <?php endforeach ?>

                </div>

            </div>

        </div>

    </div>

</section>

<?php if (!empty($block_5->text)): ?>

    <section class="text">

        <div class="container">

            <?=$block_5->text ?>

        </div>

    </section>
    
<?php endif ?>

<?php if (!empty($block_6->text)): ?>

    <section class="blocks">

        <div class="container">

            <?=$block_6->text ?>

        </div>

    </section>

<?php endif ?>

<a name="faq"></a>

<?php if (!empty($block_7->text)): ?>

    <section class="faq">

        <div class="container">

            <?=$block_7->text ?>

        </div>

    </section>

<?php endif ?>

<a name="creat"></a>

<?php if (!empty($block_8->text)): ?>

    <section class="work">

        <div class="container">

            <?=$block_8->text ?>

        </div>

    </section>

<?php endif ?>

<a name="about"></a>

<section class="about">

    <div class="container">

        <div class="row">

            <?php if (!empty($block_9->text)): ?>

                <?=$block_9->text ?>

            <?php endif ?>

            <div class="col-md-6 col-sm-12 graph-sm">

                <canvas id="myChart2" width="200"></canvas>

            </div>

        </div>

    </div>

</section>

<?php if (!empty($block_10->text)): ?>

    <section class="bottom">

        <div class="container">

            <?=$block_10->text ?>

        </div>

    </section>

<?php endif ?>

<?php if (!empty($block_11->text)): ?>

    <footer>

        <div class="container">

            <?=$block_11->text ?>

        </div>

    </footer>

<?php endif ?>

<div class="MyPopup <?=!empty($msg) ? 'show' : '' ?>" onclick="$(this).removeClass('show');">

    <div class="p-content">

        <div class="polovina_right">

            <p>Your application</p>

            <p>submitted</p>

        </div>

        <div class="clearfix"></div>

    </div>

</div>

<script>

    $(function() {

        $("#MyPopup").click(function () {

            $(this).removeClass('show');

        });

        var label = '<?=$gdata["label"] ?>';
        var labels= JSON.parse('<?=$gdata["gLabelsJson"] ?>');
        var title = JSON.parse('<?=$gdata["gtitleJson"] ?>');
        var comessions = JSON.parse('<?=$gdata["gContents1Json"] ?>');
        createGraphic1('myChart', label, labels, title, comessions);

        var label2 = '<?=$gdata["label"] ?>';
        var labels2= JSON.parse('<?=$gdata["gLabelsJson"] ?>');
        var title2 = JSON.parse('<?=$gdata["gtitleJson"] ?>');
        var comessions2 = JSON.parse('<?=$gdata["gContents2Json"] ?>');
        createGraphic2('myChart2', label2, labels2, title2, comessions2);

        $('.owl-carousel').owlCarousel({

            loop:true,

            margin:10,

            nav:true,

            responsive:{0:{items:1}, 600:{items:3}, 1000:{items:7}}
            
        });

    });

</script>

<!-- Yandex.Metrika counter -->

<script type="text/javascript" >

    (function (d, w, c) {

        (w[c] = w[c] || []).push(function() {

            try {

                w.yaCounter47493421 = new Ya.Metrika2({

                    id:47493421,

                    clickmap:true,

                    trackLinks:true,

                    accurateTrackBounce:true,

                    webvisor:true

                });

            } catch(e) { }

        });

        var n = d.getElementsByTagName("script")[0],

            s = d.createElement("script"),

            f = function () { n.parentNode.insertBefore(s, n); 

        };

        s.type = "text/javascript";

        s.async = true;

        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {

            d.addEventListener("DOMContentLoaded", f, false);

        } else { f(); }

    })(document, window, "yandex_metrika_callbacks2");

</script>

<noscript><div><img src="https://mc.yandex.ru/watch/47493421" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

<!-- /Yandex.Metrika counter -->

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113286390-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

  gtag('config', 'UA-113286390-1');

</script>