<?php

    use yii\helpers\Html;

    $this->title = $name;

?>

<h4>404</h4>

<p>he page you requested could not be found Please start searching for the information you need by beginning from the <a href="/">main</a> page of the site</p>
