<?php 

use app\models\Bdata;

?>
<?php if (!empty($nat1)): ?>

	<?php foreach ($nat1 as $key => $value): ?>

		<?php $pg = Bdata::getPage2ById($value->page_id) ?>

		<li>
			
			<a href="/<?=$pg ?>">

				<p class="l_word"><?=$txt ?></p>

				<p class="l_page"><?=$pg ?></p>
				
			</a>

		</li>

	<?php endforeach ?>
	
<?php endif ?>

<?php if (!empty($nat2)): ?>

	<?php foreach ($nat2 as $key => $value): ?>

		<?php $pg = Bdata::getPage2ById($value->page_id) ?>

		<li>
			
			<a href="#">

				<p class="l_word"><?=$txt ?></p>

				<p class="l_page"><?=$pg ?></p>
				
			</a>

		</li>

	<?php endforeach ?>
	
<?php endif ?>

<?php if (empty($nat1) AND empty($nat2)): ?>
	<li>
		<a href="#">

			<p class="l_word">&nbsp;</p>

			<p class="l_page"><?=t('Result not found') ?></p>

		</a>
	</li>
<?php endif ?>