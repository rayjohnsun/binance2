<?php 
	
?>

<?php if (!empty($block1)): ?>
	<section id="about" class="block partners-btn">
		<div class="container">
			<?=$block1->text ?>
		</div>
	</section>
<?php endif ?>

<section class="block item-graph" id="plus">

    <div class="container">

    	<h2 class="text-center"><?=t('An example of the success of our site') ?></h2>

        <canvas id="myChart" width="200"></canvas>

    </div>

</section>

<?php if (!empty($block2)): ?>
	<section class="block bg-grey" id="news">
		<div class="container">
			<?=$block2->text ?>
		</div>
	</section>
<?php endif ?>

<?php if (!empty($block_end)): ?>
	<section class="block partners-text" id="articles">
		<div class="container">
			<?=$block_end->text ?>
		</div>
	</section>
<?php endif ?>

<script>

	$(document).ready(function(){

		var label = '<?=$gdata["label"] ?>';
		var labels= JSON.parse('<?=$gdata["gLabelsJson"] ?>');
		var title = JSON.parse('<?=$gdata["gtitleJson"] ?>');
		var comessions = JSON.parse('<?=$gdata["gContents1Json"] ?>');
		createGraphic1('myChart', label, labels, title, comessions);

		$('.center').slick({
		  	centerMode: false,
		  	slidesToShow: 1,
		  	responsive: [
		    	{
		      		breakpoint: 768,
		      		settings: {
		        		arrows: false,
		        		centerMode: false,
		        		slidesToShow: 1
		      		}
		    	},
		    	{
			      	breakpoint: 480,
			      	settings: {
			        	arrows: false,
			        	centerMode: false,
			        	slidesToShow: 1
			      	}
		    	}
		  	]
		});

	});

</script>