<?php if (!empty($bmodels)): ?>

	<?php foreach ($bmodels as $key => $value): ?>

		<?php 
			$cl = '';

			if (!empty($value->sovpadenie)) {
				$cl = $value->sovpadenie == '4' ? 'hider' : 'shower';
			}
		?>
		
		<li class="<?=$cl ?>">
			<span class="logo-block">
				<img src="/<?=$value->icon ?>" alt="<?=$value->name ?>">
			</span>
			<p class="icon_plus"><?=$value->plus ?></p>
			<p class="icon_minus" style="padding-right: 0;width: 39%;"><?=$value->minus ?></p>
			<div class="link">
				<a href="<?=$value->link ?>" class="link_transform" target="_blank"><?=t('Learn More') ?></a>
			</div>
		</li>

	<?php endforeach ?>

<?php endif ?>
