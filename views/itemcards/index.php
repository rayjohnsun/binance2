<?php 

	use app\models\Bdata;

?>
<?php if (!empty($model->bonus)): ?>
	<section id="bonuses" class="block bonuses">
		<div class="container">
			<?=$model->bonus ?>
		</div>
	</section>
<?php endif ?>

<section class="block item-graph">
	<div class="container">
		<div class="row">
			<h2><?=t('Growth graph popularity') ?></h2>
			<div class="col-sm-12 col-md-12">
				
				<canvas id="myChart" width="200"></canvas>
	
			</div>
		</div>
	</div>
</section>

<?php if (!empty($model->more_card)): ?>
	<section class="block bg-table-violet">
		<div class="container">
			<?=$model->more_card ?>
		</div>
	</section>
<?php endif ?>

<?php if (!empty($model->advant_limit)): ?>
	<section class="block block-icon-top">
		<div class="container">
			<?=$model->advant_limit ?>
		</div>
	</section>
<?php endif ?>

<?php if (!empty($model->about)): ?>
	<section id="about" class="about">
		<div class="container">
			<?=$model->about ?>
		</div>
	</section>
<?php endif ?>

<section id="news" class="block bg_blue">

	<div class="container">

		<div class="row">

			<h2 class="text-center"><?=t('Last news') ?><br /><?=t('and prices crypto currency') ?></h2>
			
			<div class="col-md-6 col-sm-12 news news-js">&nbsp;</div>

			<div class="col-md-6 col-sm-12">
				
				<div class="currency">

					<div class="currency-table">

						<div class="th">

							<div style="width: 49%;"><?=t('Crypto currency') ?> <span><?=t('Name2') ?></span></div>
							
							<div style="width: 19%;"><?=t('Price') ?> <span>USD</span></div>
							
							<div style="width: 29%;"><?=t('Changes') ?> <span><?=t('in 24 hours') ?></span></div>

						</div>
						
						<div class="currency-js">&nbsp;</div>
					
					</div>
				
				</div>
				
			</div>
			
		</div>

	</div>

</section>

<?=$this->render('/layouts/_obzor_stati', ['p_id' => Bdata::PAGE_ITEM_CARD]); ?>

<script>

	$(document).ready(function(){

		loadCoins();

		loadNewsTo('.news-js');

		var label = '<?=$gdata["label"] ?>';
		var labels= JSON.parse('<?=$gdata["gLabelsJson"] ?>');
		var title = JSON.parse('<?=$gdata["gtitleJson"] ?>');
		var comessions = JSON.parse('<?=$gdata["gContents1Json"] ?>');
		createGraphic1('myChart', label, labels, title, comessions);

	});

</script>