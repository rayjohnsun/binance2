<?php

use app\models\Bdata;

    use yii\helpers\Html;
    use yii\grid\GridView;

    $this->title = 'Statya';

    $this->params['breadcrumbs'][] = $this->title;

?>
<div class="statya-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p><?= Html::a('Create Statya', ['create'], ['class' => 'btn btn-success']) ?></p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'image',
                'header' => 'Image',
                'value' => function ($m) {
                    return '<img src="/'.$m->image.'" height="50">';
                },
                'format' => 'raw',
            ],
            'id',
            [
                'attribute' => 'page_id',
                'value' => function ($m) {
                    return Bdata::getPageById($m->page_id);
                },
                'filter'=> Html::activeDropDownList($searchModel, 'page_id', Bdata::getPages(),['class'=>'form-control','prompt' => '']),
                'format' => 'raw',
            ],
            'title_en',
            'short_text',
            // 'full_text:ntext',
            [
                'attribute' => 'created_at',
                'value' => function ($m) {
                    $dt = date('d.m.Y H:i', strtotime($m->created_at));
                    return $dt;
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($m) {
                    $dt = '(not set)';
                    if (!empty($m->updated_at)) {
                        $dt = date('d.m.Y H:i', strtotime($m->updated_at));
                    }
                    return $dt;
                }
            ],
            [
                'attribute' => 'status',
                'value' => function ($m) {
                    return $m->status > 0 ? '<span class="label label-success">Активный</span>' : '<span class="label label-danger">Не активный</span>';
                },
                'format' => 'raw',
                'filter'=> Html::activeDropDownList($searchModel, 'status', [1 => 'Active', 0 => 'No active'],['class'=>'form-control','prompt' => '']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
