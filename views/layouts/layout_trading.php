<?php


	use yii\helpers\Html;
	use app\models\Bdata;
	use app\models\Pagedata;
	use app\assets\AppAsset2;

	AppAsset2::register($this);

	$banner = Pagedata::byLanAndPage(Bdata::PAGE_TRADE, Bdata::POS_START)->one();

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">

<head>
	
	<meta charset="<?= Yii::$app->charset ?>">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">

	<?= Html::csrfMetaTags() ?>

	<title><?= Html::encode($this->title) ?></title>

	<meta name="desciription" content="Buy cryptocurrency on the stock exchange Binance at a discount. Buying Bitcoin no commission. Useful information: How to build a referral network. Exchange rates. Bitcoins Drawing. More than 17583 of referrals. Already raffled 741 Bitcoins.">

    <meta name="Keywords" content="Buy cryptocurrency on the stock exchange Binance at a discount inexpensively price referral link bitcoins stock exchange dollar rate price bitcoin exchange no commission">

    <?php $this->head() ?>

	<!--[if lt IE 9]>
	    <script src="/js/html5.js"></script>
	<![endif]-->

</head>

<body id="main" class="trading">

	<ul class="soc soc-left">
		<li><a href="link" class="telegram"></a></li>
		<li><a href="link" class="fb"></a></li>
		<li><a href="link" class="tw"></a></li>
	</ul>

	<ul class="top-menu">
		<li><a href="#header">1</a></li>
		<li><a href="#about">2</a></li>
		<li><a href="#plus">3</a></li>
		<li><a href="#news">4</a></li>
		<li><a href="#articles">5</a></li>
		<li><a href="#sub">6</a></li>
	</ul>

	<header id="header">

		<div class="container">	
		
			<?=$this->render('_top_block'); ?>

			<?=$this->render('_top_menyu'); ?>

			<?php if (!empty($banner)): ?>

				<?=$banner->text ?>
				
			<?php endif ?>

		</div>

		<a href="#about" class="scroll"><img src="/images/main/scroll.png"></a>

	</header>

	<?php $this->beginBody() ?>

		<?=$content ?>

	<?php $this->endBody() ?>

	<?=$this->render('_call_to_action'); ?>
	
	<?=$this->render('_footer'); ?>
	
</body>

</html>

<?php $this->endPage() ?>