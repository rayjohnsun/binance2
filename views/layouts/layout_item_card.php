<?php

	use yii\helpers\Html;
	use app\assets\AppAsset2;
	use app\models\Pagedata;
	use app\models\Bdata;

	AppAsset2::register($this);

	$banner = $this->params['model'];

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">

<head>
	
	<meta charset="<?= Yii::$app->charset ?>">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">

	<?= Html::csrfMetaTags() ?>

	<title><?= Html::encode($this->title) ?></title>

	<meta name="desciription" content="Buy cryptocurrency on the stock exchange Binance at a discount. Buying Bitcoin no commission. Useful information: How to build a referral network. Exchange rates. Bitcoins Drawing. More than 17583 of referrals. Already raffled 741 Bitcoins.">

    <meta name="Keywords" content="Buy cryptocurrency on the stock exchange Binance at a discount inexpensively price referral link bitcoins stock exchange dollar rate price bitcoin exchange no commission">

    <?php $this->head() ?>

	<!--[if lt IE 9]>
	    <script src="/js/html5.js"></script>
	<![endif]-->

</head>

<body id="main" class="itemcard">

	<ul class="soc soc-left">
		<li><a href="link" class="telegram"></a></li>
		<li><a href="link" class="fb"></a></li>
		<li><a href="link" class="tw"></a></li>
	</ul>


	<ul class="top-menu">
		<li><a href="#header">1</a></li>
		<li><a href="#about">2</a></li>
		<li><a href="#plus">3</a></li>
		<li><a href="#news">4</a></li>
		<li><a href="#articles">5</a></li>
		<li><a href="#sub">6</a></li>
	</ul>

	<header id="header">

		<div class="container">
		
			<?=$this->render('_top_block'); ?>

			<?=$this->render('_top_menyu'); ?>
			
			<div class="row banner">
				<div class="col-sm-12 col-md-6" style="text-align: center;">
					
					<h1 class="transform"><?=$banner->name ?></h1>
					<img src="/<?=$banner->img_big ?>" class="img-card" alt="<?=$banner->name ?>" />
					<a href="link"><img src="/images/main/btn-more.png" class="img-btn" /></a>

				</div>
				<div class="col-sm-12 col-md-5 col-md-offset-1">

					<form action="" method="post" class="border-form">

						<p><?=t('More than {cnt} bitcoins were played', ['cnt' => 28]) ?></p>
						<span><?=t('Leave your data and win with us') ?></span>
						<input class="form-control" name="name" id="name" placeholder="<?=t('Subject?') ?>" type="text">
						<input class="form-control" name="name" id="pay" placeholder="<?=t('Wallet number Ethereum') ?>" type="text">
						<input class="form-control" name="address" id="address" placeholder="E-mail" type="text">
						<button type="button" data-toggle="modal" data-target="#myModal"><?=t('Become a referral') ?></button>
						
					</form>
					
				</div>
			</div>
			
		</div>

		<a href="#about" class="scroll"><img src="/images/main/scroll.png"></a>

	</header>
	
	<?php $this->beginBody() ?>

		<?=$content ?>

	<?php $this->endBody() ?>

	<?=$this->render('_call_to_action'); ?>
	
	<?=$this->render('_footer'); ?>

</body>

</html>

<?php $this->endPage() ?>