<?php 

	$langs = Yii::$app->params['languages'];

	$lan = Yii::$app->language;
	
?>
<section class="top_lan">

	<?php if (!empty($langs)): ?>
		
		<ul class="lang">
			<li class="dropdown-li">

				<a href="/language/<?=$lan ?>">
					<img src="/images/flags/<?=$lan ?>.png">
					<span><?=$langs[$lan]['id'] ?></span>
				</a>
			
				<ul class="dp_ul">

					<?php foreach ($langs as $key => $value): ?>

						<li>
							<a href="/language/<?=$key ?>">
								<img src="/images/flags/<?=$key ?>.png" alt="">
								<span><?=$value['id'] ?></span>
							</a>
						</li>
						
					<?php endforeach ?>
				</ul>
			
			</li>
			
		</ul>

	<?php endif ?>
	
	<div class="search-top">

		<div class="search_parent">

			<form action="" method="post" class="search white">
				
				<input type="text"  class="form-control" name="search" id="searchTop" placeholder="<?=t('Search in site ...') ?>">
				
				<input type="submit" class="search-btn" value="" id="searchTopBtn"/>

			</form>

			<ul class="s_content"></ul>
			
		</div>

	</div>

	<div class="logo">
		
		<img src="/images/main/logo-ub.png" alt="Universal Bitcoin" width="70" />
		<p>Universal<span>bitcoin</span></p>

	</div>

	<div class="clearfix"></div>

</section>

<?php 
	$this->registerJs('

		var el = $("#searchTop");
		var eb = $("#searchTopBtn");
		var pr = el.parent().parent().find(".s_content");

		eb.click(function(e){
			e.preventDefault();
			searchPage(el, pr);
		});

		el.keyup(function(){
			searchPage($(this), pr);
		});

		function searchPage (el, cont){

			var val = el.val();

			if(val.length > 3){
				$.ajax({
					type: "POST",
					url: "/serchpage/"+val,
					dataType: "html",
					success: function(data){
						cont.html(data);
					}
				});
			}
			else{
				pr.html("");
			}
			
		}
		
	');
?>