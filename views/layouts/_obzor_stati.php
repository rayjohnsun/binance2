<?php 

	use app\components\NHelper;

	use app\models\Statya;
	
	$st = app\models\Statya::find()->where(['>', 'status', 0])->andWhere(['page_id' => $p_id])->all();



?>

<?php if (!empty($st)): ?>

	<section id="articles" class="block map articles">

		<div class="container">

			<div class="row">

				<h2 class="text-center"><?=t('Reviews and articles') ?></h2>

				<?php foreach ($st as $key => $value): ?>

					<div class="col-sm-12 col-md-4" style="margin-bottom: 10px;">

						<article class="item">

							<a href="/blog-item/<?=$value->id ?>">

								<?php if (!empty($value->image)): ?>
									
									<img src="/<?=$value->image ?>" />
								
								<?php endif ?>

							</a>

							<p><a href="/blog-item/<?=$value->id ?>"><?=$value->title ?></a></p>

							<span><a href="/blog-item/<?=$value->id ?>"><?=$value->short_text ?></a></span>

							<a href="/blog-item/<?=$value->id ?>"><?=t('Read more ...') ?></a>

							<ins><?=NHelper::vremya(!empty($value->updated_at) ? $value->updated_at : $value->created_at); ?></ins>

						</article>

					</div>
					
				<?php endforeach ?>

			</div>

		</div>
		
	</section>

<?php endif ?>