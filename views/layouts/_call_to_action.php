<section id="sub" class="block sub">

	<div class="container">

		<div class="row">

			<div class="col-sm-12 col-md-4 col-md-offset-1">

				<form action="/site/calltoaction" method="post" id="SubscribeMeForm">

					<input type="hidden" name="<?=\Yii::$app->request->csrfParam?>" value="<?=\Yii::$app->request->csrfToken?>"/>

					<p><?=t('Subscribe!') ?></p>

					<span><?=t('And keep abreast of the news of the crypto-currency market') ?></span>

					<input class="form-control" name="Subscribers[imya]" id="name-sub" placeholder="<?=t('Name') ?>" type="text">

					<input class="form-control" name="Subscribers[email]" id="address-sub" placeholder="E-mail" type="text">

					<button type="submit" id="SubscribeMe"><?=t('Receive news first') ?></button>

				</form>

			</div>

		</div>

	</div>

</section>

<script>
	
	$(document).ready(function () {

		var form = $('#SubscribeMeForm');

		form.validate({

			rules: {

				'Subscribers[imya]': "required",

				'Subscribers[email]': {

					required: true,

					email: true,

				}

			},

			messages: {

				'Subscribers[imya]': "Please enter Name",

				'Subscribers[email]': "Please enter the correct email",

			},

			submitHandler: function(form) {

				var f = $(form);

            	$.ajax({

            		type: f.attr('method'),

            		url: f.attr('action'),

            		dataType: 'json',

            		data: f.serialize(),

            		success: function (data) {

            			if (data.status > 0) {

            				f.find('input[type="text"]').val('');

            			}

            			alert(data.message);
            			
            		},

            	});

            	return false;

            },

		});
		
	});

</script>