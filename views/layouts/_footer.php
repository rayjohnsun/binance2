<?php 

	use app\models\Bdata;
	use app\models\Pagedata;
	use app\models\Categories;

	$menu = Categories::getMenyuItems('bottom');
	$foot = Pagedata::byLanAndPage(Bdata::PAGE_BUY, Bdata::POS_END)->one();
	
?>
<footer>
	<div class="container">

		<?php if (!empty($menu)): ?>

			<div class="row">

				<div class="col-sm-12 col-md-3">

				<?php foreach ($menu as $key => $value): ?>

					<p><?=$value->name ?> <ins>?</ins></p>
					<?php if (!empty($value->submenu)): ?>
						<ul>
							<?php foreach ($value->submenu as $key2 => $value2): ?>
								<?php if ($value2->bottom > 0): ?>
									<li>
										<a href="<?=$value2->link ?>"><?=$value2->name ?></a>
									</li>
								<?php endif ?>
							<?php endforeach ?>
						</ul>
					<?php endif ?>

					<?php if ($value->inline_with_next < 1): ?>
				</div>

				<div class="col-sm-12 col-md-3">
					<?php endif ?>
					
				<?php endforeach ?>

				</div>

			</div>
			
		<?php endif ?>

		<?php if (!empty($foot)): ?>

			<?=$foot->text ?>

		<?php endif ?>
		
	</div>
	
</footer>