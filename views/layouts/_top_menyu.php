<?php 

	use app\models\Categories;

	
	$menu = Categories::getMenyuItems('top');	
	
?>
<?php if (!empty($menu)): ?>

	<div class="navbar-header top_menu">

		<nav class="navbar nav navbar-default">
		
			<div class="container-fluid">
		
				<div class="navbar-header">
					
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-main">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
					
				</div>
				
				<div class="collapse navbar-collapse" id="navbar-main">		
					<ul>

						<?php foreach ($menu as $key => $value): ?>

							<?php if (!empty($value->submenu)): ?>

								<li class="dropdown-li">

									<?php if (!empty($value->link)): ?>

										<a href="<?=$value->link ?>"><?=$value->name ?></a>
										
									<?php else: ?>

										<?=$value->name ?>

									<?php endif ?>

									<ul>

										<?php if (!empty($value->submenu)): ?>
											
											<?php foreach ($value->submenu as $key2 => $value2): ?>

												<li>

													<a href="<?=$value2->ssilka ?>"><?=$value2->name ?></a>
													
												</li>
												
											<?php endforeach ?>

										<?php endif ?>

									</ul>

								</li>

							<?php else: ?>

								<li>

									<?php if (!empty($value->link)): ?>

										<a href="<?=$value->link ?>"><?=$value->name ?></a>
										
									<?php else: ?>

										<?=$value->name ?>
										
									<?php endif ?>
									
								</li>

							<?php endif ?>

						<?php endforeach ?>
						
					</ul>
				</div>
				
			</div>	
		
		</nav>

	</div>

<?php endif ?>