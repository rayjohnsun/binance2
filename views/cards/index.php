<?php 

	use app\models\Bdata;

?>
<section id="about" class="about">

	<?php if (!empty($block1)): ?>

		<div class="container">
			
			<?=$block1->text ?>

		</div>

	<?php endif ?>

</section>

<section class="graph">

    <div class="container">

        <canvas id="myChart" width="200"></canvas>

    </div>

</section>

<?php if (!empty($block2)): ?>
	
	<section id="plus" class="block ulcards">

		<div class="container">

			<h2 class="text-center"><?=t('The most popular cryptographic cards<br />in Europe and the USA') ?></h2>

			<ul>
				
				<?php foreach ($block2 as $key => $value): ?>

					<li class="col-sm-12 col-md-6">
						
						<img src="/<?=$value->img_small ?>" alt="<?=$value->name ?>" />

						<span style="<?=!empty($value->color) ? 'background: '.$value->color.';' : ''; ?>" class="name-card"><?=$value->name ?></span>

						<?=$value->plus_minus ?>

					</li>
					
				<?php endforeach ?>

			</ul>
			
		</div>

	</section>
	
<?php endif ?>

<section id="news" class="block bg_blue">

	<div class="container">

		<div class="row">

			<h2 class="text-center"><?=t('Last news') ?><br /><?=t('and prices crypto currency') ?></h2>
			
			<div class="col-md-6 col-sm-12 news news-js">&nbsp;</div>
				
			<div class="col-md-6 col-sm-12">
				
				<div class="currency">

					<div class="currency-table">

						<div class="th">

							<div style="width: 49%;"><?=t('Crypto currency') ?> <span><?=t('Name2') ?></span></div>
							
							<div style="width: 19%;"><?=t('Price') ?> <span>USD</span></div>
							
							<div style="width: 29%;"><?=t('Changes') ?> <span><?=t('in 24 hours') ?></span></div>

						</div>
						
						<div class="currency-js">&nbsp;</div>
					
					</div>
				
				</div>
				
			</div>
			
		</div>

	</div>

</section>

<?=$this->render('/layouts/_obzor_stati', ['p_id' => Bdata::PAGE_CARDS]); ?>

<script>

	$(document).ready(function(){

		loadCoins();

		loadNewsTo('.news-js');

		var label = '<?=$gdata["label"] ?>';
		var labels= JSON.parse('<?=$gdata["gLabelsJson"] ?>');
		var title = JSON.parse('<?=$gdata["gtitleJson"] ?>');
		var comessions = JSON.parse('<?=$gdata["gContents1Json"] ?>');
		createGraphic1('myChart', label, labels, title, comessions);

	});

</script>