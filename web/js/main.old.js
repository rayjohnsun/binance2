$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:7
        }
    }
})
	
		// Ждем когда загрузится DOM
$(function() {
  // Инициализация плагина на форме
  // Форма имеет атрибут "registration"
  $("form[name='creatform']").validate({
    // Правила проверки полей
    rules: {
      // Ключ с левой стороны это название полей формы.
      // Правила валидации находятся с правой стороны
      subject: "required",
      text: "required",
		  email: {
        required: true,
        email: true
      }     
    },
    // Установка сообщений об ошибке
    messages: {
      subject: "Please enter your subject",
      text: "Please enter your text",
		  email: "Please enter your email",      
    },
    submitHandler: function(form) {
      // console.log($(form).serialize());
      // $.ajax({
      //   type: 'POST',
      //   url: '/ajax_email.php',
      //   dataType: 'json',
      //   data: $(form).serialize(),
      //   success: function (data) {
      //     console.log(data);
      //     if (data.status > 0) {
      //       $("#MyPopup").show();
      //       $(form).find('input').val('');
      //     }
      //     else{
      //       alert('All fields are can not be empty');
      //     }
      //   }
      // });
      form.submit();
    }
  });
});