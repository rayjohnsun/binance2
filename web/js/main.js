$(function() {

    if ($("form[name='creatform']").length > 0) {

        $("form[name='creatform']").validate({

            rules: {
                login: "required",
                address: "required",
        		email: {
                    required: true,
                    email: true
                }         
            },

            messages: {
                login: "Please enter your login",
                address: "Please enter your wallet address",
        	    email: "Please enter your email"
              
            },

            submitHandler: function(form) {

              form.submit();

            }

        });
        
    }

});

function normalizeString(txt) {

    var txt2 = txt.split("/").join("....");

    txt2 = txt2.split("%").join("");

    return txt2;

}

function loadBirja(txt) {

    var cont = $('#BirjaContent');

    var txt2 = normalizeString(txt);

    $.ajax({

        type: 'get',

        url: '/buy/birja/'+txt2,

        dataType: 'html',

        success: function (data) {

            cont.html(data);

        }

    });

}

function loadBirjaByCoins(txt) {

    var cont = $('#BirjaContent');

    var txt2 = normalizeString(txt);

    $.ajax({

        type: 'get',

        url: '/trading/birja/'+txt2,

        dataType: 'html',

        success: function (data) {

            cont.html(data);

        }

    });

}

function loadNewsTo(element) {
    var cont = $(element);
    if (cont.length > 0) {

        $.ajax({
            type: 'get',
            url: '/site/loadnews',
            dataType: 'html',
            success: function (data) {
                cont.html(data);
                cont.slick({
                    infinite: true,
                    vertical: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                });
            }
        });

    }
    else{
        console.log('elemet not set');
    }
}

function loadCoins() {

    var cont = $('.currency-js');

    $.ajax({
        type: 'get',
        url: '/site/loadcoins',
        dataType: 'html',
        success: function (data) {
            cont.html(data);
            cont.slick({
                infinite: true,
                vertical: true,
                slidesToShow: 6,
                slidesToScroll: 1
            });
        }
    });
}

function formatState (state) {

    if (!state.id) { return state.text; }

    var $state = $('<span ><img sytle="display: inline-block;" src="/images/main/' + state.element.value.toLowerCase() + '.png" /> ' + state.text + '</span>');

    return $state;
        
}

function createGraphic1(id, label, labels, title, comessions) {
    
    var ctx = document.getElementById(id);

    var myChart = new Chart(ctx, {

        type: 'line',

        data: {

            labels: labels,

            title: title,

            datasets: [{

                label: label,

                data: comessions,

                backgroundColor: [

                    'rgba(103, 93, 148, 1)'

                ],

                borderColor: [

                    'rgba(245,245,245,1)'

                ],

                borderWidth: 1

            }]

        },

        options: {

            scales: {

                yAxes: [{

                    ticks: {

                        beginAtZero:true,

                        suggestedMax: 6,

                        stepSize: 1,

                    }

                }],

                xAxes:[{

                    ticks: {

                        labelOffset: 26

                    }

                }]

            },

            tooltips: {

                callbacks: {

                    title: function (tooltipItem, data) {

                        var index   = tooltipItem[0].index;

                        var txt     = data.title[index];

                        return txt;

                    },

                    label: function (tooltipItem, data) {

                        return 'Total commission: '+tooltipItem.yLabel;

                    }

                }

            }

        }

    });

}

function createGraphic2(id, label, labels, title, comessions) {

    var ctx     = document.getElementById(id);

    var myChart = new Chart(ctx, {

        type: 'line',

        data: {

            labels: labels,

            title: title,

            datasets: [{

                label: label,

                data: comessions,

                backgroundColor: [

                    'rgba(103, 93, 148, 1)'

                ],

                borderColor: [

                    'rgba(245,245,245,1)'

                ],

                borderWidth: 1

            }]

        },

        options: {

            scales: {

                yAxes: [{

                    ticks: {

                        beginAtZero:true,

                        stepSize: 400,

                        suggestedMax: 2000,

                    }

                }],

                xAxes:[{

                    ticks: {

                        labelOffset: 20

                    }

                }]

            },

            tooltips: {

                callbacks: {

                    title: function (tooltipItem, data) {

                        var index   = tooltipItem[0].index;

                        var txt     = data.title[index];

                        return txt;

                    },

                    label: function (tooltipItem, data) {

                        return 'Total referers: '+tooltipItem.yLabel;

                    }

                }

            }

        }

    });

}

