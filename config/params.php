<?php

	return [

	    'adminEmail'=> 'admin@example.com',

	    'languages' => [
	    	'ru' 	=> ['id' => 'RU', 'name' => 'Русский'],
	    	'en' 	=> ['id' => 'EN', 'name' => 'English'],
	    	'ar' 	=> ['id' => 'AR', 'name' => 'العربية'],
	    	'de' 	=> ['id' => 'DE', 'name' => 'Deutsch'],
	    	'es' 	=> ['id' => 'ES', 'name' => 'Español'],
	    	'it' 	=> ['id' => 'IT', 'name' => 'italiano'],
	    ],

	    'user.passwordResetTokenExpire' => 3600,

	    'commission'=> [
	    	'PAGE_INDEX' 	=> ['from' => 0.001,	'to' => 0.08], // можно дробной число
	    	'PAGE_BUY' 		=> ['from' => 0.002,	'to' => 0.09], // можно дробной число
	    	'PAGE_MAIN' 	=> ['from' => 0.001,	'to' => 0.08], // можно дробной число
	    	'PAGE_MINING' 	=> ['from' => 0.001,	'to' => 0.08], // можно дробной число
	    	'PAGE_TRADE' 	=> ['from' => 0.001,	'to' => 0.08], // можно дробной число
	    	'PAGE_CARDS' 	=> ['from' => 0.001,	'to' => 0.08], // можно дробной число
	    	'PAGE_ITEM_CARD'=> ['from' => 0.001,	'to' => 0.08], // можно дробной число
	    	'PAGE_ITEM_CARD2'=> ['from' => 0.001,	'to' => 0.08], // можно дробной число
	    	'PAGE_BLOG'		=> ['from' => 0.001,	'to' => 0.08], // можно дробной число
	    	'PAGE_ABOUT'	=> ['from' => 0.001,	'to' => 0.08], // можно дробной число
    	],

		'peoples'	=> [
			'PAGE_INDEX' 	=> ['from' => 5, 		'to' => 50], // только целой число
			'PAGE_BUY' 		=> ['from' => 6, 		'to' => 60], // только целой число
			'PAGE_MAIN' 	=> ['from' => 5, 		'to' => 50], // только целой число
			'PAGE_MINING' 	=> ['from' => 5, 		'to' => 50], // только целой число
			'PAGE_TRADE' 	=> ['from' => 5, 		'to' => 50], // только целой число
			'PAGE_CARDS' 	=> ['from' => 5, 		'to' => 50], // только целой число
			'PAGE_ITEM_CARD'=> ['from' => 5, 		'to' => 50], // только целой число
			'PAGE_ITEM_CARD2'=> ['from' => 5, 		'to' => 50], // только целой число
			'PAGE_BLOG'		=> ['from' => 5, 		'to' => 50], // только целой число
			'PAGE_ABOUT'	=> ['from' => 5, 		'to' => 50], // только целой число
		],
		
		'trader'	=> [
			'PAGE_INDEX' 	=> ['from' => 300000,	'to' => 500000], // только целой число
			'PAGE_BUY' 		=> ['from' => 400000,	'to' => 600000], // только целой число
			'PAGE_MAIN' 	=> ['from' => 300000,	'to' => 500000], // только целой число
			'PAGE_MINING' 	=> ['from' => 300000,	'to' => 500000], // только целой число
			'PAGE_TRADE' 	=> ['from' => 300000,	'to' => 500000], // только целой число
			'PAGE_CARDS' 	=> ['from' => 300000,	'to' => 500000], // только целой число
			'PAGE_ITEM_CARD'=> ['from' => 300000,	'to' => 500000], // только целой число
			'PAGE_ITEM_CARD2'=> ['from' => 300000,	'to' => 500000], // только целой число
			'PAGE_BLOG'		=> ['from' => 300000,	'to' => 500000], // только целой число
			'PAGE_ABOUT'	=> ['from' => 300000,	'to' => 500000], // только целой число
		],

		'coin_image'=> '/images/coinssmall/',

		'list_coin' => [
			'BTC' 	=> ['id' => 'Bitcoin', 				'icon' => 'btc.png'],
			'ETH' 	=> ['id' => 'Ethereum', 			'icon' => 'eth.png'],
			'EOS' 	=> ['id' => 'EOS', 					'icon' => 'eos.png'],
			'BCH' 	=> ['id' => 'Bitcoin Cash / BCC', 	'icon' => 'bch.jpg'],
			'XRP' 	=> ['id' => 'Ripple', 				'icon' => 'xrp.png'],
			'LTC' 	=> ['id' => 'Litecoin', 			'icon' => 'ltc.png'],
			'TRX' 	=> ['id' => 'Tronix', 				'icon' => 'trx.png'],
			'HT' 	=> ['id' => 'Huobi Token', 			'icon' => 'ht.png'],
			'ETC' 	=> ['id' => 'Ethereum Classic', 	'icon' => 'etc.png'],
			'BNB'	=> ['id' => 'Binance Coin', 		'icon' => 'bnb.png'],	
			'XVG' 	=> ['id' => 'Verge', 				'icon' => 'xvg.png'],
			'NEO' 	=> ['id' => 'NEO', 					'icon' => 'neo.jpg'],
			'DASH' 	=> ['id' => 'DigitalCash', 			'icon' => 'dash.png'],
			'QTUM' 	=> ['id' => 'QTUM', 				'icon' => 'qtum.png'],
			'ICX' 	=> ['id' => 'ICON Project', 		'icon' => 'icx.png'],
			'ONT' 	=> ['id' => 'Ontology', 			'icon' => 'ont.jpg'],
			'IOT' 	=> ['id' => 'IOTA', 				'icon' => 'iot.png'],
			'XMR' 	=> ['id' => 'Monero', 				'icon' => 'xmr.png'],
			'XLM' 	=> ['id' => 'Stellar', 				'icon' => 'xlm.png'],
			'VEN' 	=> ['id' => 'Vechain', 				'icon' => 'ven.png'],
			'ZEC' 	=> ['id' => 'ZCash', 				'icon' => 'zec.png'],
			'IOST' 	=> ['id' => 'IOS token', 			'icon' => 'iost.png'],
			'ADA' 	=> ['id' => 'Cardano', 				'icon' => 'ada.png'],
			'ELF' 	=> ['id' => 'aelf', 				'icon' => 'elf.png'],
			'ABT' 	=> ['id' => 'ArcBlock', 			'icon' => 'abt.png'],
			'OCN' 	=> ['id' => 'Odyssey', 				'icon' => 'ocn.png'],
			'BTM*' 	=> ['id' => 'Bytom', 				'icon' => 'btm_.png'],
			'WAVES' => ['id' => 'Waves', 				'icon' => 'waves.png'],
			'OMG' 	=> ['id' => 'OmiseGo', 				'icon' => 'omg.png'],
			'DGD' 	=> ['id' => 'Digix DAO', 			'icon' => 'dgd.png'],
		],

	];
