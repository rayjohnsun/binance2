<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

function t($message, $attr = []){return Yii::t('app', $message, $attr);}
function short($string, $max = 255){
   if(mb_strlen($string, 'utf-8') >= $max){
       $string = mb_substr($string, 0, $max - 1, 'utf-8').'...';
   } 
   return $string;
}

$config = [
    'id' => 'basic',
    'language' => 'en',
    'sourceLanguage' => 'en-US',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'nlanguage'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'baseUrl' => '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'UfKlTUQ3Yfm6Uj1F49LZIKIRbBAwn1ed',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ''          => 'main/index',
                'index'     => 'main/index',
                'main'      => 'main/index',
                // 'contact'   => 'site/contact',
                'login'     => 'site/login',
                'logout'    => 'site/logout',

                'blog-item/<id:\d+>'     => 'blog/item',
                'blog/<page:\d+>'       => 'blog/index',
                'blog'                  => 'blog/index',

                'itemcard/<id:\d+>'     => 'itemcards/index',
                'itemcard2/<id:\d+>'    => 'itemcards/index2',
                'list/<id:\d+>'         => 'itemcards/index3',
                'language/<lan:\w+>'    => 'site/language',
                'serchpage/<txt>'       => 'site/serchpage',
                '<controller:buy>/<action:birja>/<txt:\w+>' => 'buy/birja',
                '<controller:trading>/<action:birja>/<txt>' => 'trading/birja',
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                ],
                'yii\web\YiiAsset' => [
                    'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                ],
            ],
        ],
        'nlanguage' => ['class' => 'app\components\NLanguage'],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'fileMap' => [
                        'app'       => 'app.php',
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    // $config['bootstrap'][] = 'debug';
    // $config['modules']['debug'] = [
    //     'class' => 'yii\debug\Module',
    //     // uncomment the following to add your IP if you are not connecting from localhost.
    //     //'allowedIPs' => ['127.0.0.1', '::1'],
    // ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
