<?php 

	namespace app\components;

	use yii\base\Component;
	use Yii;

	class NLanguage extends Component {
		
		public function init() {

			$session 	= Yii::$app->session;

			$languages 	= Yii::$app->params['languages'];


			if ($session->has('nlanguage')) {

				Yii::$app->language = $session->get('nlanguage');

			} else{

				$language = strtolower(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));

				if (isset($languages[$language])) {
					
					Yii::$app->language = $language;

					$session->set('nlanguage', $language);
					
				} else {

					$session->set('nlanguage', Yii::$app->language);

				}

			}

			parent::init();
			
		}
		
	}