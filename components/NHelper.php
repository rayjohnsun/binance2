<?php 

	namespace app\components;

	class NHelper {
		
		public static function tryGetMonthName($num) {
			
			$num = (int)$num;

			$months = [

				1 	=> 'January',

				2 	=> 'February',

				3 	=> 'March',

				4 	=> 'April',

				5 	=> 'May',

				6 	=> 'June',

				7 	=> 'July',

				8 	=> 'August',

				9 	=> 'September',

				10 	=> 'October',

				11 	=> 'November',

				12 	=> 'December',

			];

			if (isset($months[$num])) {

				return $months[$num];

			}
			
			return $num;

		}

		public static function normalizeString($txt) {

			$txt = str_replace('....', '/', $txt);

			$txt = str_replace(',', ' ', $txt);

			$txt = str_replace('\'', '', $txt);

			$txt = str_replace("\"", '', $txt);

			$txt = str_replace('%', '', $txt);

			$txt = str_replace('!', '', $txt);

			$txt = trim($txt);

			return $txt;

		}

		public static function vremya($time) {
			
			$txt = '';

			$now = strtotime('now');

			$tim = strtotime($time);

			$dif = ($now - $tim);

			if ($dif >= 0) {

				
				if ($dif < 60) {

					$txt = t('{number} seconds ago', ['number' => $dif]);
					
				} elseif ($dif >= 60 AND $dif < 3600) {

					$dif2 = floor($dif / 60);

					$txt = t('{number} minutes ago', ['number' => $dif2]);
					
				} elseif ($dif >= 3600 AND $dif < 86400) {

					$dif2 = floor($dif / 3600);

					$txt = t('{number} hours ago', ['number' => $dif2]);
					
				} elseif ($dif >= 86400 AND $dif < 2592000) {

					$dif2 = floor($dif / 86400);

					$txt = t('{number} days ago', ['number' => $dif2]);
					
				} elseif ($dif >= 2592000 AND $dif < 31536000) {

					$dif2 = floor($dif / 2592000);

					$txt = t('{number} months ago', ['number' => $dif2]);
					
				} else{

					$dif2 = floor($dif / 31536000);

					$txt = t('{number} years ago', ['number' => $dif2]);

				}

			} else{

				$txt = t('{number} seconds ago', ['number' => 0]);

			}

			return $txt;
			
		}

		public static function createPagenation($cnt_current, $page, $link) {			
			$paging = [];
			$cnt_pg = ($page <= 1 || $page >= $cnt_current) ? 2 : 1;
			if ($cnt_current > 1) {
				$limit_left = $page - $cnt_pg - 2;
				$limit_right= $cnt_current - $page - $cnt_pg - 1;
				$i = 1;
				while ($i <= $cnt_current) { 
					$i2 = $i > 1 ? $i : '';
					$page_lnk = '/'.$link.'/'.$i2;

					if ($limit_left <= 0 AND $limit_right <= 0) {
						$paging[] = ['link' => $page_lnk, 'title' => $i, 'active' => $i == $page];
					}
					elseif ($limit_left > 0 AND $limit_right > 0) {
						if ($i == 1) {
							$paging[] = ['link' => $page_lnk, 'title' => $i, 'active' => $i == $page];
							$paging[] = ['link' => '', 'title' => '...', 'active' => false];
						}
						elseif ($i == $cnt_current) {
							$paging[] = ['link' => '', 'title' => '...', 'active' => false];
							$paging[] = ['link' => $page_lnk, 'title' => $i, 'active' => $i == $page];
						}
						elseif ($i >= $page - $cnt_pg AND $i <= $page + $cnt_pg) {
							$paging[] = ['link' => $page_lnk, 'title' => $i, 'active' => $i == $page];
						}
					}
					elseif ($limit_left > 0 AND $limit_right <= 0) {
						if ($i == 1) {
							$paging[] = ['link' => $page_lnk, 'title' => $i, 'active' => $i == $page];
							$paging[] = ['link' => '', 'title' => '...', 'active' => false];
						}
						elseif ($i >= $page - $cnt_pg) {
							$paging[] = ['link' => $page_lnk, 'title' => $i, 'active' => $i == $page];
						}
					}
					elseif ($limit_right > 0 AND $limit_left <= 0) {
						if ($i == $cnt_current) {
							$paging[] = ['link' => '', 'title' => '...', 'active' => false];
							$paging[] = ['link' => $page_lnk, 'title' => $i, 'active' => $i == $page];
						}
						elseif ($i <= $page + $cnt_pg) {
							$paging[] = ['link' => $page_lnk, 'title' => $i, 'active' => $i == $page];
						}
					}
					$i++;
				}
			}
			return $paging;
		}

	}