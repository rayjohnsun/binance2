<?php 

	namespace app\components;

	class Coins {
		
		protected $api = 'https://min-api.cryptocompare.com/data/price';
		protected $api2= 'https://min-api.cryptocompare.com/data/pricemultifull';
		protected $api3= 'https://min-api.cryptocompare.com/data/news/';
		public $fsym;
		public $fsyms;
		public $tsyms;
		public $result;
		protected $ch;
		public $interval;
		public $time_file;
		public $lan;

		public function getDBTime() {
			return DB::one('SELECT * FROM b_time');
		}

		public function refreshDBTime() {
			if (!empty($this->interval)) {
				$now = strtotime('+'.$this->interval);
				$res = $this->getDBTime();
				$sql = "INSERT INTO b_time (btime, updated_at) VALUES (:btime, :updated)";
				if (!empty($res)) {
					$sql = "UPDATE b_time SET btime = :btime, updated_at = :updated";
				}
				$success = DB::prepare($sql, [':btime' => $now, ':updated' => date('Y-m-d H:i:s')]);
				return $success;
			}
			throw new Exception("interval is missing", 1);
		}

		public function refreshTime() {
			if (!empty($this->interval)) {
				if (!empty($this->time_file)) {
					$now = strtotime('+'.$this->interval);
					$file = fopen($this->time_file,"w");
					fwrite($file, $now);
					fclose($file);
					return true;
				}
				throw new Exception("time_file is missing", 1);
			}
			throw new Exception("interval is missing", 1);
		}

		public function timeDBIsLeft() {
			if (!empty($this->interval)) {
				$res = $this->getDBTime();
				$now = strtotime('NOW');
				if (!empty($res)) {
					$content = (int)$res['btime'];
					$date = date('Y-m-d', $content);
					$date_int = strtotime($date);
					if ($now <= $date_int) {
						return false;
					}
					return true;
				}
				return true;
			}
			throw new Exception("interval is missing", 1);
		}

		public function timeIsLeft() {
			if (!empty($this->interval)) {
				if (!empty($this->time_file)) {
					if (file_exists($this->time_file)) {
						$content = (int)file_get_contents($this->time_file);
						$now = strtotime('NOW');
						if ($now <= $content) {
							return false;
						}
						return true;
					}
					return true;
				}
				throw new Exception("time_file is missing", 1);
				
			}
			throw new Exception("interval is missing", 1);
		}

		public function open() {
			$this->ch = curl_init();
			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		}

		public function close() {
			curl_close($this->ch);
		}

		public function execute() {
			if (!empty($this->fsym)) {
				if (!empty($this->tsyms)) {
					curl_setopt($this->ch, CURLOPT_URL, $this->api.'?fsym='.$this->fsym.'&tsyms='.$this->tsyms);
					$data = curl_exec($this->ch);
					$this->result = json_decode($data, true);
					return true;
				}
				throw new Exception("tsyms is missing", 1);
			}
			throw new Exception("fsym is missing", 1);
		}

		public function execute2() {
			if (!empty($this->fsyms)) {
				if (!empty($this->tsyms)) {
					curl_setopt($this->ch, CURLOPT_URL, $this->api2.'?fsyms='.$this->fsyms.'&tsyms='.$this->tsyms);
					$data = curl_exec($this->ch);
					$this->result = json_decode($data, true);
					return true;
				}
				throw new Exception("tsyms is missing", 1);
			}
			throw new Exception("fsyms is missing", 1);
		}

		public function execute3() {
			if (!empty($this->lan)) {
				curl_setopt($this->ch, CURLOPT_URL, $this->api3.'?lang='.$this->lan);
				$data = curl_exec($this->ch);
				$this->result = json_decode($data, true);
				return true;
			}
			throw new Exception("lan is missing", 1);
		}

		public function getPrice($val = null) {
			if (array_key_exists($val, $this->result)) {
				return $this->result[$val];
			}
			return $this->result;
		}

		public function getMulti($one = true) {
			if (!empty($this->result)) {
				if (array_key_exists('RAW', $this->result) AND array_key_exists('DISPLAY', $this->result)) {
					$array = [];
					foreach ($this->result['RAW'] as $key => $value) {
						if ($one == true) {
							foreach ($value as $key2 => $value2) {
								$display = $this->result['DISPLAY'][$key][$key2];
								$array[$key] = ['raw' => $value2, 'display' => $display];
								break;
							}
						}
						else{
							foreach ($value as $key2 => $value2) {
								$display = $this->result['DISPLAY'][$key][$key2];
								$array[$key][$key2] = ['raw' => $value2, 'display' => $display];
							}
						}
					}
					return $array;
				}
				return [];
			}
			throw new Exception("result is missing", 1);
		}

	}