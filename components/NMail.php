<?php 

	namespace app\components;

	class NMail {

		public $to;

		public $subject;

		public $text;

		public $email;
		
		public function send() {

			$headers = 'MIME-Version: 1.0' . "\r\n";

			$headers .= 'Content-type: text/plain; charset=utf-8' . "\r\n";

			$headers .= 'From:  ' . ' <' . $this->email .'>' . " \r\n" . 'Reply-To: '.  $this->email . "\r\n" . 'X-Mailer: PHP/' . phpversion();

			return mail($this->to, $this->subject, $this->text, $headers);

		}

	}