<?php

namespace app\assets;

use yii\web\AssetBundle;

class AppAssetAdmin extends AssetBundle {

    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $css = [

        '/css/bootstrap.min.css',
        
        // 'https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700',

        // '/css/responsive.css',

        // '/css/owl.carousel.min.css',

        '/css/admin.css',

    ];

    public $js = [

        '/js/bootstrap.min.js',

        // '/js/owl.carousel.min.js',

        // '/js/jquery.validate.min.js',

        // '/js/fontawesome-all.min.js',

        // '/js/Chart.min.js',

        // '/js/main.js',

    ];
    
    public $jsOptions = [

        'position' => \yii\web\View::POS_HEAD

    ];

    public $depends = [

        'yii\web\YiiAsset',

    ];

}
