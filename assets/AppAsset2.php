<?php

namespace app\assets;

use yii\web\AssetBundle;

class AppAsset2 extends AssetBundle {

    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $css = [

        '/css/bootstrap.min.css',

        '/css/style.css',

        '/css/owl.carousel.min.css',

        '/css/slick.css',
        
        // '/css/select2.min.css',

        '/css/responsive.css',

        'https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700',

    ];

    public $js = [

        'https://code.jquery.com/ui/1.12.1/jquery-ui.js',

        '/js/bootstrap.min.js',

        '/js/owl.carousel.min.js',

        '/js/jquery.validate.min.js',

        '/js/slick.min.js',
        
        // '/js/select2.min.js',
        '/js/Chart.min.js',
        
        '/js/main.js',

        '/js/main2.js',
        
        'http://slow-spot.surge.sh/js/fontawesome-all.min.js',

    ];
    
    public $jsOptions = [

        'position' => \yii\web\View::POS_HEAD

    ];

    public $depends = [

        'yii\web\YiiAsset',

    ];

}
