<?php

namespace app\controllers;

use Yii;
use app\models\Admcards;
use app\models\Categories;
use app\models\SearchCategories;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CategoryController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {

        $this->layout = 'admin';

        return parent::beforeAction($action);

    }

    public function actionIndex() {

        $searchModel = new SearchCategories();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $mod = Categories::find()->select(['id', 'name_en'])->where(['parent_id' => 0])->all();

        $mod = ArrayHelper::map($mod, 'id', 'name_en');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'mod' => $mod,
        ]);

    }

    public function actionView($id) {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);

    }

    public function actionCreate() {

        $model = new Categories();

        $model->status = 1;
        $model->norder = rand(0,50);
        $model->top = 1;
        $model->bottom = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['index']);

        }

        return $this->render('create', [
            'model' => $model,
            'page_list' => Admcards::findByLan(),
        ]);

    }

    public function actionUpdate($id) {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['index']);

        }

        $page_list = Admcards::findByLan();

        return $this->render('update', [
            'model' => $model,
            'page_list' => $page_list,
        ]);

    }

    public function actionDelete($id) {

        $this->findModel($id)->delete();

        return $this->redirect(['index']);

    }

    protected function findModel($id) {

        if (($model = Categories::findOne($id)) !== null) {

            return $model;

        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
    
}
