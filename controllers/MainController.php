<?php

namespace app\controllers;

use Yii;
use app\models\Bdata;
use app\models\Birja;
use app\models\Pagedata;
use yii\db\Expression;
use yii\web\Controller;

class MainController extends Controller {

    public function actionIndex() {

        $this->layout = 'layout_main';

        $bdata = new Bdata();

        $gdata = $bdata->getGraphic(Bdata::PAGE_MAIN);

        $page1 = Pagedata::byLanAndPage(Bdata::PAGE_MAIN, Bdata::POS_BLOCK1)->one();
        $page2 = Pagedata::byLanAndPage(Bdata::PAGE_MAIN, Bdata::POS_BLOCK2)->one();

        return $this->render('index', ['gdata' => $gdata, 'page1' => $page1, 'page2' => $page2]);

    }

}
