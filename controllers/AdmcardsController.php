<?php

namespace app\controllers;

use Yii;
use app\models\Admcards;
use app\models\SearchAdmcards;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class AdmcardsController extends Controller {

    public function behaviors() {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];

    }

    public function beforeAction($action) {

        $this->layout = 'admin';

        return parent::beforeAction($action);

    }

    public function actionIndex() {

        $searchModel = new SearchAdmcards();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionView($id) {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);

    }

    public function actionCreate() {

        $model = new Admcards();

        $model->status = 1;

        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            $model->imageFileLarge = UploadedFile::getInstance($model, 'imageFileLarge');

            if ($model->imageFile && $model->imageFileLarge && $model->validate()) {

                if ($model->upload()) {

                    $model->imageFile = null;

                    if ($model->uploadLarge()) {

                        $model->imageFileLarge = null;
                        
                        $model->save(false);

                        return $this->redirect(['update', 'id' => $model->id]);
                        
                    }
                    
                } else {

                    if (!empty($model->img_small)) {

                        @unlink($model->img_small);

                    }

                }
                
            }

        }

        return $this->render('create', ['model' => $model]);

    }

    public function actionUpdate($id) {

        $model = $this->findModel($id);

        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            $model->imageFileLarge = UploadedFile::getInstance($model, 'imageFileLarge');

            if ($model->validate()) {

                if ($model->imageFile) {
                    
                    $model->upload();

                    $model->imageFile = null;
                }

                if ($model->imageFileLarge) {
                    
                    $model->uploadLarge();

                    $model->imageFileLarge = null;
                }

                $model->save(false);

                return $this->redirect(['update', 'id' => $model->id]);

            }

        }

        return $this->render('update', ['model' => $model]);

    }

    public function actionDelete($id) {

        $this->findModel($id)->delete();

        return $this->redirect(['index']);

    }

    protected function findModel($id) {

        if (($model = Admcards::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

}
