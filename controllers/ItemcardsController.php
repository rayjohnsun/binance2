<?php

namespace app\controllers;

use Yii;
use app\models\Admcards;
use app\models\Bdata;
use app\models\Birja;
use app\models\Pagedata;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ItemcardsController extends Controller {

    public function actionIndex($id) {

    	if ($model = Admcards::findOne($id) AND $model->status > 0) {
    		
	        $this->layout = 'layout_item_card';

	        $this->view->params['model'] = $model;

	        $bdata = new Bdata();

	        $gdata = $bdata->getGraphic(Bdata::PAGE_ITEM_CARD);

	        return $this->render('index', ['gdata' => $gdata, 'model' => $model]);

    	}

    	throw new NotFoundHttpException('The requested page does not exist.');

    }

    public function actionIndex2($id) {

        if ($model = Admcards::findOne($id) AND $model->status > 0) {
            
            $this->layout = 'layout_item_card2';

            $this->view->params['model'] = $model;

            return $this->render('index2', ['model' => $model]);

        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    public function actionIndex3($id) {

    	if ($model = Birja::findOne($id) AND $model->status > 0) {
    		
	        $this->layout = 'layout_list';

	        $this->view->params['model'] = $model;

	        return $this->render('index3', ['model' => $model]);

    	}

    	throw new NotFoundHttpException('The requested page does not exist.');

    }

}
