<?php

namespace app\controllers;

use Yii;
use app\components\Coins;
use app\components\NHelper;
use app\components\NMail;
use app\models\Bdata;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\Pagedata;
use app\models\Statya;
use app\models\Subscribers;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class SiteController extends Controller {

    public function behaviors() {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];

    }

    public function beforeAction($action) {

        if (parent::beforeAction($action)) {

            if ($action->id=='error'){

                $this->layout ='layout_error';

            } 

            return true;

        } else {

            return false;

        }

    }

    public function actions() {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];

    }

    public function actionIndex() {

        $this->layout = 'layout_index';

        $msg = $this->sendFormIfIsset();

        /**************************CURRENCIES***************************/

        $Coins  = new Coins();

        $Coins->interval = '1 day';
        
        $currencies = []; $i = 0;

        $Coins->open();

        $Coins->fsym    = 'DASH';

        $Coins->tsyms   = 'BTC,USD,RUB,EUR';

        $Coins->execute();

        $pr1 = number_format((float)$Coins->getPrice('BTC'),2,'.',',');

        $pr2 = number_format((float)$Coins->getPrice('USD'),2,'.',',');

        $pr3 = number_format((float)$Coins->getPrice('RUB'),2,'.',',');

        $pr4 = number_format((float)$Coins->getPrice('EUR'),2,'.',',');

        $currencies[$i] = ['from' => 'DASH', 'to' => 'BTC', 'price' => $pr1]; $i++;

        $currencies[$i] = ['from' => 'DASH', 'to' => 'USD', 'price' => $pr2]; $i++;

        $currencies[$i] = ['from' => 'DASH', 'to' => 'RUB', 'price' => $pr3]; $i++;

        $currencies[$i] = ['from' => 'DASH', 'to' => 'EUR', 'price' => $pr4]; $i++;

        $Coins->fsym = 'ETH';

        $Coins->tsyms = 'BTC,LTC,USD,EUR';

        $Coins->execute();

        $ps1 = number_format((float)$Coins->getPrice('BTC'),2,'.',',');

        $ps2 = number_format((float)$Coins->getPrice('LTC'),2,'.',',');

        $ps3 = number_format((float)$Coins->getPrice('USD'),2,'.',',');

        $ps4 = number_format((float)$Coins->getPrice('EUR'),2,'.',',');

        $currencies[$i] = ['from' => 'ETH', 'to' => 'BTC', 'price' => $ps1]; $i++;

        $currencies[$i] = ['from' => 'ETH', 'to' => 'LTC', 'price' => $ps2]; $i++;

        $currencies[$i] = ['from' => 'ETH', 'to' => 'USD', 'price' => $ps3]; $i++;

        $currencies[$i] = ['from' => 'ETH', 'to' => 'EUR', 'price' => $ps4]; $i++;

        $Coins->close();

        $Coins->open();

        $Coins->fsyms   = 'BTC';

        $Coins->tsyms   = 'USD';

        $Coins->execute2();

        $Coins->close();

        $coin = $Coins->getMulti();

        $apl1 = $apl2 = 0;

        if (!empty($coin)) {

            $apl1 = (float)$coin['BTC']['display']['CHANGEPCT24HOUR'];

            if ($apl1 > 0) {

                $apl1 = '+'.$apl1;

            }

            $apl2 = number_format($coin['BTC']['raw']['PRICE'],1,'.',' ');

        }

        /****************************TRADER****************************/

        $trader_data    = Bdata::find()->orderBy(['bdate' => SORT_DESC])->limit(1)->one();

        $active_trader  = $trader_data->trader;

        /******************************Graphgic*********************************/        

        $bdata = new Bdata();

        $gdata = $bdata->getGraphic(Bdata::PAGE_INDEX);

        /*****************************Blocks***********************************/

        $block_menu = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_MENU)->one();
        $block_start= Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_START)->one();
        $block_1    = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_BLOCK1)->one();
        $block_2    = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_BLOCK2)->one();
        $block_3    = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_BLOCK3)->one();
        $block_4    = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_BLOCK4)->one();
        $block_5    = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_BLOCK5)->one();
        $block_6    = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_BLOCK6)->one();
        $block_7    = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_BLOCK7)->one();
        $block_8    = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_BLOCK8)->one();
        $block_9    = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_BLOCK9)->one();
        $block_10   = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_BLOCK10)->one();
        $block_11   = Pagedata::byLanAndPage(Bdata::PAGE_INDEX, Bdata::POS_BLOCK11)->one();

        return $this->render('index', [
            'msg'           => $msg,
            'apl1'          =>$apl1,
            'apl2'          =>$apl2,
            'active_trader' => $active_trader,
            'currencies'    => $currencies,
            'gdata'         => $gdata,
            'block_menu'    => $block_menu,
            'block_start'   => $block_start,
            'block_1'       => $block_1,
            'block_2'       => $block_2,
            'block_3'       => $block_3,
            'block_4'       => $block_4,
            'block_5'       => $block_5,
            'block_6'       => $block_6,
            'block_7'       => $block_7,
            'block_8'       => $block_8,
            'block_9'       => $block_9,
            'block_10'      => $block_10,
            'block_11'      => $block_11,
        ]);

    }

    public function actionLanguage($lan) {

        if (isset(Yii::$app->params['languages'][$lan])) {

            Yii::$app->session->set('nlanguage', $lan);

            Yii::$app->language = $lan;
            
        }

        return $this->redirect(!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : '/');
        
    }

    public function sendFormIfIsset() {

        $msg = "";

        if (Yii::$app->request->post('Message')) {
            
            $post = Yii::$app->request->post('Message');

            $mail = new NMail();

            $mail->to = 'binancereferral1@gmail.com';

            $mail->subject = $post['subject'];

            $mail->text = $post['text'];

            $mail->email = $post['email'];
            
            if (!empty($mail->subject) AND !empty($mail->text) AND !empty($mail->email)) {
                
                if ($mail->send()) {

                    if ($model2 = Subscribers::find()->where(['email' => $mail->email, 'page' => Yii::$app->request->referrer])->one()) {

                        $model2->imya = $mail->subject;
                        $model2->adres = $mail->text;
                        $model2->save();

                    } else {

                        $model = new Subscribers();
                        $model->imya    = $mail->subject;
                        $model->email   = $mail->email;
                        $model->status  = 1;
                        $model->page    = Yii::$app->request->referrer;
                        $model->adres   = $mail->text;
                        $model->save();

                    }
                    
                    $msg = "Successfully sended your message!";

                }

            }

        }

        return $msg;
        
    }

    public function actionLogin() {

        $this->layout = 'layout_buy';

        if (!Yii::$app->user->isGuest) {

            return $this->goHome();

        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->goBack();

        }

        $model->password = '';

        return $this->render('login', ['model' => $model]);

    }

    public function actionLogout() {

        Yii::$app->user->logout();

        return $this->goHome();

    }

    // public function actionContact() {

    //     $model = new ContactForm();

    //     if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {

    //         Yii::$app->session->setFlash('contactFormSubmitted');

    //         return $this->refresh();

    //     }

    //     return $this->render('contact', [

    //         'model' => $model,

    //     ]);

    // }

    public function actionLoadnews() {

        if (Yii::$app->request->isAjax) {

            $Coins  = new Coins();

            $Coins->open();

            $Coins->lan = 'EN';

            $Coins->execute3();

            $Coins->close();

            return $this->renderAjax('_loadnews', ['nat' => $Coins->result]);

        } else {

            return $this->goHome();

        }
        
    }

    public function actionLoadcoins() {

        if (Yii::$app->request->isAjax) {

            $config = Yii::$app->params;

            $list30 = $config['list_coin'];

            $coinimg= $config['coin_image'];

            $fsyms  = implode(',', array_keys($list30));

            $Coins  = new Coins();

            $Coins->open();

            $Coins->fsyms   = $fsyms;

            $Coins->tsyms   = 'USD';

            $Coins->execute2();

            $Coins->close();

            $res = $Coins->getMulti(true);

            $nat = [];

            foreach ($list30 as $key => $value) {

                $nat[$key] = $value;

                $nat[$key]['price'] = $nat[$key]['up_dow'] = $nat[$key]['change'] = $nat[$key]['img'] = '';

                if (isset($res[$key])) {
                    
                    $nat[$key]['price'] = @$res[$key]['display']['PRICE'];

                    $symbol = @$res[$key]['display']['TOSYMBOL'];

                    $change = @$res[$key]['raw']['CHANGE24HOUR'];

                    $nat[$key]['up_dow']= $change >= 0 ? 'up' : 'down';

                    $nat[$key]['change']= number_format($change, 2, '.', '').$symbol;

                    $nat[$key]['img'] = $coinimg . $value['icon'];

                }

            }

            return $this->renderAjax('_loadcoins', ['nat' => $nat]);

        } else {

            return $this->goHome();

        }

    }

    public function actionCalltoaction() {

        if (Yii::$app->request->isAjax) {

            $data['status'] = 0;

            $model = new Subscribers();

            if ($model->load(Yii::$app->request->post())) {
                
                if ($model2 = Subscribers::find()->where(['email' => $model->email, 'page' => Yii::$app->request->referrer])->one()) {

                    $model2->imya   = $model->imya;
                    $model2->save();

                } else {

                    $model->page    = Yii::$app->request->referrer;
                    $model->status  = 1;
                    $model->save();

                }

                $data['status'] = 1;

                $data['message'] = "You are successfully subscribed";

            } else {

                $data['message'] = "Error! Try again please";

            }

            return json_encode($data);
            
        } else {

            return $this->goHome();

        }
        
    }

    public function actionSerchpage($txt = '') {

        if (Yii::$app->request->isAjax) {

            if (strlen($txt) > 3) {

                $lan = Yii::$app->language;

                $p_title = 'title_'.$lan;
                $p_text = 'text_'.$lan;

                // statya => 'title', 'short', 'full'

                $nat1 = Pagedata::find()->select([$p_title, $p_text, 'page_id'])
                    ->where(['status' => 1])
                    ->andWhere(['OR', 
                        ['like', $p_title, $txt], 
                        ['like', $p_text, $txt]
                    ])
                    ->groupBy('page_id')->all();

                $nat2 = Statya::find()->select(['title', 'short_text', 'page_id'])
                    ->where(['status' => 1])
                    ->andWhere(['OR',
                        ['like', 'title', $txt],
                        ['like', 'short_text', $txt]
                    ])
                    ->groupBy('page_id')->all();
                
                return $this->renderAjax('_searchpage', ['nat1' => $nat1, 'nat2' => $nat2, 'txt' => $txt]);

            } else {
                return '';
            }
            
        } else {

            return $this->goHome();

        }
        
    }

}
