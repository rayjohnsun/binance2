<?php

namespace app\controllers;

use Yii;
use app\components\NHelper;
use app\models\Bdata;
use app\models\Birja;
use app\models\Pagedata;
use yii\db\Expression;
use yii\web\Controller;

class TradingController extends Controller {

    public function actionIndex() {

        $this->layout = 'layout_trading';

        $bdata = new Bdata();

        $gdata = $bdata->getGraphic(Bdata::PAGE_TRADE);

        $block1 = Pagedata::byLanAndPage(Bdata::PAGE_TRADE, Bdata::POS_BLOCK1)->one();

        return $this->render('index', ['gdata' => $gdata, 'block1' => $block1]);

    }

    public function actionBirja($txt = '') {

        if (Yii::$app->request->isAjax) {

            if (strlen($txt) > 0) {

            	$txt = NHelper::normalizeString($txt);

                $sovpadenie = "(CASE WHEN coins = '".$txt."' THEN 0 WHEN coins LIKE '".$txt."%' THEN 1 WHEN coins LIKE '%".$txt."%' THEN 2 WHEN coins LIKE '%".$txt."' THEN 3 ELSE 4 END) AS sovpadenie";

                $ord = new Expression("CASE WHEN coins = '".$txt."' THEN 0 WHEN coins LIKE '".$txt."%' THEN 1 WHEN coins LIKE '%".$txt."%' THEN 2 WHEN coins LIKE '%".$txt."' THEN 3 ELSE 4 END, coins ASC");

                $bmodels = Birja::find()->select('*, '.$sovpadenie)->orderBy([$ord])->limit(10);
                
            } else {

                $bmodels = Birja::find()->limit(10);
            
            }

            $bmodels = $bmodels->all();

            return $this->renderAjax('birja', [

                'bmodels' => $bmodels,

            ]);

        } else {

            return $this->redirect('/buy');

        }
        
    }

}
