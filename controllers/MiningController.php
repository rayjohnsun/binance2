<?php

namespace app\controllers;

use Yii;
use app\models\Bdata;
use app\models\Birja;
use app\models\Pagedata;
use yii\db\Expression;
use yii\web\Controller;

class MiningController extends Controller {

    public function actionIndex() {

        $this->layout = 'layout_mining';

        $bdata = new Bdata();

        $gdata = $bdata->getGraphic(Bdata::PAGE_MINING);

        $block1 = Pagedata::byLanAndPage(Bdata::PAGE_MINING, Bdata::POS_BLOCK1)->one();
        $block2 = Pagedata::byLanAndPage(Bdata::PAGE_MINING, Bdata::POS_BLOCK2)->one();

        return $this->render('index', ['gdata' => $gdata, 'block1' => $block1, 'block2' => $block2]);

    }

}
