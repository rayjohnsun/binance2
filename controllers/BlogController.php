<?php

namespace app\controllers;

use Yii;
use app\components\NHelper;
use app\models\Bdata;
use app\models\Pagedata;
use app\models\Statya;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BlogController extends Controller {

    public function actionIndex($page = 0) {

        $this->layout = 'layout_blog';

        $lim = 15;

        $cnt = Statya::find()->count();

        $cnt_current = ceil($cnt / $lim);

    	$page = ((int)$page > 0 AND (int)$page <= $cnt_current) ?  (int)$page : 1;

        $off = $lim * ($page - 1);

        $models = Statya::find()->where(['status' => 1])->orderBy(['id' => SORT_DESC])->limit($lim)->offset($off)->all();

        $pagenation = NHelper::createPagenation($cnt_current, $page, 'blog');

        $block1 = Pagedata::byLanAndPage(Bdata::PAGE_CARDS, Bdata::POS_BLOCK1)->one();

        return $this->render('index', ['block1' => $block1, 'models' => $models, 'pagenation' => $pagenation]);

    }

    public function actionItem($id) {
        
        if ($model = Statya::findOne($id)) {

            $this->layout = 'layout_blog_item';

            return $this->render('item', ['model' => $model]);
            
        }

        throw new NotFoundHttpException('The requested page does not exist.');
        
    }

}
