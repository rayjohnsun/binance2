<?php

namespace app\controllers;

use Yii;
use app\models\Bdata;
use app\models\Pagedata;
use yii\web\Controller;

class AboutController extends Controller {

    public function actionIndex() {

        $this->layout = 'layout_about';

        $bdata = new Bdata();

        $gdata = $bdata->getGraphic(Bdata::PAGE_ABOUT);

        $block1 = Pagedata::byLanAndPage(Bdata::PAGE_ABOUT, Bdata::POS_BLOCK1)->one();

        $block2 = Pagedata::byLanAndPage(Bdata::PAGE_ABOUT, Bdata::POS_BLOCK2)->one();

        $block_end = Pagedata::byLanAndPage(Bdata::PAGE_ABOUT, Bdata::POS_END)->one();

        return $this->render('index', ['gdata' => $gdata, 'block1' => $block1, 'block2' => $block2, 'block_end' => $block_end]);

    }

}
