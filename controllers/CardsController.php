<?php

namespace app\controllers;

use Yii;
use app\models\Admcards;
use app\models\Bdata;
use app\models\Birja;
use app\models\Pagedata;
use yii\db\Expression;
use yii\web\Controller;

class CardsController extends Controller {

    public function actionIndex() {

        $this->layout = 'layout_cards';

        $bdata = new Bdata();

        $gdata = $bdata->getGraphic(Bdata::PAGE_CARDS);

        $block1 = Pagedata::byLanAndPage(Bdata::PAGE_CARDS, Bdata::POS_BLOCK1)->one();
        $block2 = Admcards::find()->select(['id', 'name', 'color', 'img_small', 'link', 'plus_minus'])->where(['status' => 1])->all();

        return $this->render('index', ['gdata' => $gdata, 'block1' => $block1, 'block2' => $block2]);

    }

}
